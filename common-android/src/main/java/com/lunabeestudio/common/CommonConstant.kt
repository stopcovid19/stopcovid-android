/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common

import java.util.Locale

object CommonConstant {

    val SUPPORTED_LOCALES: Array<Locale> = arrayOf(Locale.UK, Locale.FRANCE)

    const val DEFAULT_LANGUAGE: String = "en"

    object SharedPrefs {
        const val USER_LANGUAGE: String = "User.Language"
        const val PrevSyncLanguageFeaturedInfo: String = "Featured.Info.Last.Language.Sync"
        const val CHOSEN_POSTAL_CODE: String = "Chosen.Postal.Code"
    }

    object FeaturedInfo {
        const val FILE_PREFIX_THUMBNAIL: String = "featured_info_thumbnail_"
        const val FILE_PREFIX_HD: String = "featured_info_hd_"
    }
}
