/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/22 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common.extension

import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.domain.model.DepartmentKeyFigureMap
import com.lunabeestudio.domain.model.FigureType
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.KeyFigureChartType
import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.domain.model.KeyFigureSeriesItem
import keynumbers.Keynumbers

fun Keynumbers.KeyNumbersMessage.toKeyFigures(): List<KeyFigure> = keyfigureListList.mapIndexed { index, figure ->
    figure.toKeyFigure(index)
}

fun Keynumbers.KeyNumbersMessage.KeyfigureMessage.toKeyFigure(index: Int): KeyFigure = KeyFigure(
    index,
    this.category,
    this.labelKey,
    this.valueGlobalToDisplay,
    this.valueGlobal,
    this.isFeatured,
    this.isHighlighted,
    this.extractDate.toLong(),
    this.valuesDepartmentsList.map { departmentValuesMessage ->
        DepartmentKeyFigure(
            departmentValuesMessage.dptNb,
            departmentValuesMessage.dptLabel,
            departmentValuesMessage.extractDate.toLong(),
            departmentValuesMessage.value,
            departmentValuesMessage.valueToDisplay,
            departmentValuesMessage.seriesList.map { message ->
                message.toKeyFigureSeriesItem()
            },
        )
    },
    this.displayOnSameChart,
    this.limitLine,
    this.chartType?.takeIf { it.isNotEmpty() }?.let { safeEnumValueOf<KeyFigureChartType>(it) } ?: KeyFigureChartType.LINES,
    this.seriesList.map { message ->
        message.toKeyFigureSeriesItem()
    },
    this.avgSeriesList.map { message ->
        message.toKeyFigureSeriesItem()
    },
    this.magnitude,
    this.isFeatured,
    index.toFloat(),
    this.figType?.let { safeEnumValueOf<FigureType>(it) },
)

private fun Keynumbers.KeyNumbersMessage.ElementSerieMessage.toKeyFigureSeriesItem() = KeyFigureSeriesItem(
    this.date.toLong(),
    this.value,
    this.opacity,
)

fun Keynumbers.KeyNumbersMessage.toMapKeyFigures(): List<KeyFigureMap> = keyfigureMapListList.map {
    it.toKeyFigureMap()
}

fun Keynumbers.KeyNumbersMessage.KeyfigureMapMessage.toKeyFigureMap(): KeyFigureMap = KeyFigureMap(
    this.labelKey,
    this.version,
    this.valuesDepartmentsList.map {
        DepartmentKeyFigureMap(
            it.dptNb,
            it.dptLabel,
            it.seriesList.map { message ->
                message.toKeyFigureSeriesItem()
            },
        )
    },
)
