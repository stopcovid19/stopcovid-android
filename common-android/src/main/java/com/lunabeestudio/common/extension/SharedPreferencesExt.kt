/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common.extension

import android.content.SharedPreferences
import androidx.core.content.edit
import com.lunabeestudio.common.CommonConstant

// TODO Move to local-android module
var SharedPreferences.userLanguage: String?
    get() = getString(CommonConstant.SharedPrefs.USER_LANGUAGE, null)
    set(value) = edit { putString(CommonConstant.SharedPrefs.USER_LANGUAGE, value) }

var SharedPreferences.prevSyncLanguageFeaturedInfo: String?
    get() = getString(CommonConstant.SharedPrefs.PrevSyncLanguageFeaturedInfo, null)
    set(value) = edit { putString(CommonConstant.SharedPrefs.PrevSyncLanguageFeaturedInfo, value) }

var SharedPreferences.chosenPostalCode: String?
    get() = getString(CommonConstant.SharedPrefs.CHOSEN_POSTAL_CODE, null)
    set(value) = edit { putString(CommonConstant.SharedPrefs.CHOSEN_POSTAL_CODE, value) }

val SharedPreferences.hasChosenPostalCode: Boolean
    get() = chosenPostalCode != null
