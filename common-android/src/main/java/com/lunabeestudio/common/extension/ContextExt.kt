/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common.extension

import android.content.Context
import androidx.preference.PreferenceManager
import com.lunabeestudio.common.CommonConstant
import java.util.Locale

// TODO move to local-android module
fun Context.getApplicationLanguage(): String {
    val userLanguage = PreferenceManager.getDefaultSharedPreferences(this).userLanguage
    val supportedLanguages = CommonConstant.SUPPORTED_LOCALES.map { it.language }
    return userLanguage ?: if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        val locales = resources.configuration.locales.toList()
        locales.firstOrNull { supportedLanguages.contains(it.language) }?.language ?: CommonConstant.DEFAULT_LANGUAGE
    } else {
        Locale.getDefault().language.takeIf { supportedLanguages.contains(it) } ?: CommonConstant.DEFAULT_LANGUAGE
    }
}
