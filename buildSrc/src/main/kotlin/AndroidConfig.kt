/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/5/17 - for the TOUS-ANTI-COVID project
 */

object AndroidConfig {
    private const val VERSION_CODE: Int = 584
    private const val VERSION_NAME: String = "7.0.0"

    const val COMPILE_SDK: Int = 33
    const val TARGET_SDK: Int = COMPILE_SDK
    const val MIN_APP_SDK: Int = 21

    val envVersionCode: Int = System.getenv(EnvConfig.ENV_VERSION_CODE)?.toInt() ?: VERSION_CODE
    val envVersionName: String = System.getenv(EnvConfig.ENV_VERSION_NAME) ?: VERSION_NAME
}
