/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.file

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.lunabeestudio.domain.di.IODispatcher
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.lang.reflect.Type
import javax.inject.Inject

class JsonLocalDataSourceImpl @Inject constructor(
    @IODispatcher private val ioDispatcher: CoroutineDispatcher,
    private val fileLocalDataSource: FileLocalDataSource,
) : JsonLocalDataSource {
    private val gson: Gson = Gson()

    override suspend fun <T> loadLocal(localFileName: String, assetFilePath: String?, type: Type): T? {
        return withContext(ioDispatcher) {
            fileLocalDataSource.getLocalFileOrAssetStream(localFileName, assetFilePath)?.use {
                it.readBytes()
            }?.toString(Charsets.UTF_8)?.let { fileString ->
                try {
                    gson.fromJson<T>(fileString, type)
                } catch (e: Exception) {
                    Timber.e(e)
                    null
                }
            }
        }
    }

    override suspend fun <T> fileNotCorrupted(file: File, type: Type): Boolean {
        return withContext(ioDispatcher) {
            try {
                val fileString = file.readText()
                if (fileString.isBlank()) {
                    Timber.e("Fetched blank file ${file.name}")
                    false
                } else {
                    gson.fromJson<T>(fileString, type)
                    true
                }
            } catch (e: JsonSyntaxException) {
                false
            }
        }
    }
}
