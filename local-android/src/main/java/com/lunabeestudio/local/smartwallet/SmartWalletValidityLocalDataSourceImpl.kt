/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.smartwallet

import com.google.gson.reflect.TypeToken
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.repository.smartwallet.SmartWalletValidityLocalDataSource
import com.lunabeestudio.repository.smartwallet.model.ApiSmartWalletValidityPivot
import java.lang.reflect.Type
import javax.inject.Inject

class SmartWalletValidityLocalDataSourceImpl @Inject constructor() : SmartWalletValidityLocalDataSource {
    override val type: Type
        get() = object : TypeToken<List<ApiSmartWalletValidityPivot>>() {}.type

    override fun getLocalFileName(): String {
        return ConfigConstant.SmartWallet.EXPIRATION_FILENAME
    }

    override fun getRemoteFileUrl(): String {
        return ConfigConstant.SmartWallet.URL + getLocalFileName()
    }

    override fun getAssetFilePath(): String {
        return ConfigConstant.SmartWallet.FOLDER + getLocalFileName()
    }
}
