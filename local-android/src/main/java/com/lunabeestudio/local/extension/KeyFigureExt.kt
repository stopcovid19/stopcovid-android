/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import com.lunabeestudio.domain.model.FigureType
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.KeyFigureChartType
import com.lunabeestudio.local.keyfigure.model.FigureTypeRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureChartTypeRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesItemRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesTypeRoom

internal fun KeyFigure.toKeyFigureRoom(): KeyFigureRoom = KeyFigureRoom(
    labelKey,
    index,
    category,
    valueGlobalToDisplay,
    valueGlobal,
    isFeatured,
    isHighlighted,
    extractDateS,
    displayOnSameChart,
    limitLine,
    chartType.toKeyFigureChartTypeRoom(),
    magnitude,
    figureType?.toFigureTypeRoom(),
    series?.map { it.toKeyFigureSeriesItemRoom(labelKey, null, KeyFigureSeriesTypeRoom.NAT) },
    avgSeries?.map { it.toKeyFigureSeriesItemRoom(labelKey, null, KeyFigureSeriesTypeRoom.AVG) },
)

internal fun KeyFigureRoom.toKeyFigure(
    isFavorite: Boolean?,
    favoriteOrder: Float?,
    series: List<KeyFigureSeriesItemRoom>?,
): KeyFigure = KeyFigure(
    index,
    category,
    labelKey,
    valueGlobalToDisplay,
    valueGlobal,
    isFeatured,
    isHighlighted,
    extractDateS,
    null,
    displayOnSameChart,
    limitLine,
    chartType.toKeyFigureChartType(),
    series?.filter { it.serieType == KeyFigureSeriesTypeRoom.NAT }?.map(KeyFigureSeriesItemRoom::toKeyFigureSeriesItem),
    series?.filter { it.serieType == KeyFigureSeriesTypeRoom.AVG }?.map(KeyFigureSeriesItemRoom::toKeyFigureSeriesItem),
    magnitude,
    isFavorite ?: isFeatured,
    favoriteOrder ?: index.toFloat(),
    figureType?.toFigureType(),
)

private fun KeyFigureChartTypeRoom.toKeyFigureChartType(): KeyFigureChartType {
    return when (this) {
        KeyFigureChartTypeRoom.BARS -> KeyFigureChartType.BARS
        KeyFigureChartTypeRoom.LINES -> KeyFigureChartType.LINES
    }
}

private fun KeyFigureChartType.toKeyFigureChartTypeRoom(): KeyFigureChartTypeRoom {
    return when (this) {
        KeyFigureChartType.BARS -> KeyFigureChartTypeRoom.BARS
        KeyFigureChartType.LINES -> KeyFigureChartTypeRoom.LINES
    }
}

private fun FigureType.toFigureTypeRoom(): FigureTypeRoom {
    return when (this) {
        FigureType.R -> FigureTypeRoom.R
        FigureType.PERC -> FigureTypeRoom.PERC
        FigureType.PERCK -> FigureTypeRoom.PERCK
        FigureType.FLOAT -> FigureTypeRoom.FLOAT
        FigureType.INT -> FigureTypeRoom.INT
    }
}

private fun FigureTypeRoom.toFigureType(): FigureType {
    return when (this) {
        FigureTypeRoom.R -> FigureType.R
        FigureTypeRoom.PERC -> FigureType.PERC
        FigureTypeRoom.PERCK -> FigureType.PERCK
        FigureTypeRoom.FLOAT -> FigureType.FLOAT
        FigureTypeRoom.INT -> FigureType.INT
    }
}
