/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.info

import com.lunabeestudio.domain.model.InfoCenterData
import com.lunabeestudio.local.info.model.InfoCenterMappedEntryRoom
import com.lunabeestudio.repository.infocenter.InfoCenterLocalDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class InfoCenterLocalDataSourceImpl @Inject constructor(
    private val infoCenterDao: InfoCenterDao,
) : InfoCenterLocalDataSource {
    override fun getPastInfoCenterDataWithoutTag(amount: Int, now: Long): Flow<InfoCenterData> {
        return infoCenterDao.getPastInfoCenterDataWithoutTag(amount, now).map { roomList ->
            InfoCenterData(roomList.map(InfoCenterMappedEntryRoom::toInfoCenterEntry))
        }
    }

    override fun getPastInfoCenterData(now: Long): Flow<InfoCenterData> =
        infoCenterDao.getPastInfoCenterData(now).map { roomList ->
            val tagsById: Map<String, List<InfoCenterData.InfoCenterEntry.InfoCenterTag?>> = roomList.groupBy(
                { it.id },
                { tempRoom ->
                    val label = tempRoom.tagLabel
                    val color = tempRoom.tagColor
                    if (label != null && color != null) {
                        InfoCenterData.InfoCenterEntry.InfoCenterTag(label, color)
                    } else {
                        null
                    }
                },
            )
            val entries = roomList.distinctBy { it.id }.mapNotNull {
                InfoCenterData.InfoCenterEntry(
                    id = it.id.hashCode().toLong(),
                    title = it.title ?: return@mapNotNull null,
                    description = it.description ?: return@mapNotNull null,
                    buttonLabel = it.buttonLabel,
                    url = it.url,
                    timestamp = it.timestamp,
                    tags = tagsById[it.id]?.filterNotNull(),
                )
            }
            InfoCenterData(entries)
        }
}
