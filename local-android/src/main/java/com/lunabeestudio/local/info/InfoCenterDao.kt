/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.info

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.lunabeestudio.local.info.model.InfoCenterEntryRoom
import com.lunabeestudio.local.info.model.InfoCenterEntryTagCrossRefRoom
import com.lunabeestudio.local.info.model.InfoCenterLabelRoom
import com.lunabeestudio.local.info.model.InfoCenterMappedEntryRoom
import com.lunabeestudio.local.info.model.InfoCenterMappedEntryWithTagsRoom
import com.lunabeestudio.local.info.model.InfoCenterTagRoom
import kotlinx.coroutines.flow.Flow

@Dao
interface InfoCenterDao {
    /*
     * INSERT
     */
    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(
        infoCenterEntryRoom: List<InfoCenterEntryRoom>,
        infoCenterLabelRoom: List<InfoCenterLabelRoom>,
        infoCenterTagRoom: List<InfoCenterTagRoom>,
        infoCenterEntryTagCrossRefRoom: List<InfoCenterEntryTagCrossRefRoom>,
    )

    @Query("DELETE FROM InfoCenterEntryRoom")
    fun deleteInfoCenterEntryRoom()

    @Query("DELETE FROM InfoCenterLabelRoom")
    fun deleteInfoCenterLabelRoom()

    @Query("DELETE FROM InfoCenterTagRoom")
    fun deleteInfoCenterTagRoom()

    @Query("DELETE FROM InfoCenterEntryTagCrossRefRoom")
    fun deleteInfoCenterEntryTagCrossRefRoom()

    @Transaction
    fun replaceAll(
        infoCenterEntryRoom: List<InfoCenterEntryRoom>,
        infoCenterLabelRoom: List<InfoCenterLabelRoom>,
        infoCenterTagRoom: List<InfoCenterTagRoom>,
        infoCenterEntryTagCrossRefRoom: List<InfoCenterEntryTagCrossRefRoom>,
    ) {
        if (infoCenterEntryRoom.isNotEmpty()) {
            deleteInfoCenterEntryRoom()
        }
        if (infoCenterLabelRoom.isNotEmpty()) {
            deleteInfoCenterLabelRoom()
        }
        if (infoCenterTagRoom.isNotEmpty()) {
            deleteInfoCenterTagRoom()
        }
        if (infoCenterEntryTagCrossRefRoom.isNotEmpty()) {
            deleteInfoCenterEntryTagCrossRefRoom()
        }

        insertAll(infoCenterEntryRoom, infoCenterLabelRoom, infoCenterTagRoom, infoCenterEntryTagCrossRefRoom)
    }

    /*
     * QUERY
     */
    @Query(
        """
            SELECT 
                InfoCenterEntryRoom.infoCenterEntryId as id,
                TitleLabels.label as title,
                DescriptionLabels.label as description,
                ButtonLabels.label as buttonLabel,
                UrlLabels.label as url,
                InfoCenterEntryRoom.timestamp
            FROM InfoCenterEntryRoom
            JOIN InfoCenterLabelRoom TitleLabels 
                ON InfoCenterEntryRoom.titleKey = TitleLabels.infoCenterLabelId
            JOIN InfoCenterLabelRoom DescriptionLabels 
                ON InfoCenterEntryRoom.descriptionKey = DescriptionLabels.infoCenterLabelId
            LEFT JOIN InfoCenterLabelRoom ButtonLabels 
                ON InfoCenterEntryRoom.buttonLabelKey = ButtonLabels.infoCenterLabelId
            LEFT JOIN InfoCenterLabelRoom UrlLabels 
                ON InfoCenterEntryRoom.urlKey = UrlLabels.infoCenterLabelId
            WHERE InfoCenterEntryRoom.timestamp <= :now
            ORDER BY InfoCenterEntryRoom.timestamp DESC
            LIMIT :amount
    """,
    )
    fun getPastInfoCenterDataWithoutTag(amount: Int, now: Long): Flow<List<InfoCenterMappedEntryRoom>>

    @Query(
        """
        SELECT 
            InfoCenterEntryRoom.infoCenterEntryId as id,
            TitleLabels.label as title,
            DescriptionLabels.label as description, 
            ButtonLabels.label as buttonLabel, 
            UrlLabels.label as url,
            InfoCenterEntryRoom.timestamp,
            InfoCenterLabelRoom.label as tagLabel, 
            InfoCenterTagRoom.colorCode as tagColor
        FROM InfoCenterEntryRoom
        JOIN InfoCenterLabelRoom TitleLabels 
            ON InfoCenterEntryRoom.titleKey = TitleLabels.infoCenterLabelId
        JOIN InfoCenterLabelRoom DescriptionLabels 
            ON InfoCenterEntryRoom.descriptionKey = DescriptionLabels.infoCenterLabelId
        LEFT JOIN InfoCenterLabelRoom ButtonLabels 
            ON InfoCenterEntryRoom.buttonLabelKey = ButtonLabels.infoCenterLabelId
        LEFT JOIN InfoCenterLabelRoom UrlLabels 
            ON InfoCenterEntryRoom.urlKey = UrlLabels.infoCenterLabelId
        LEFT JOIN InfoCenterEntryTagCrossRefRoom 
            ON InfoCenterEntryRoom.infoCenterEntryId = InfoCenterEntryTagCrossRefRoom.infoCenterEntryId
        LEFT JOIN InfoCenterTagRoom 
            ON InfoCenterEntryTagCrossRefRoom.tagLabelKey = InfoCenterTagRoom.tagLabelKey
        LEFT JOIN InfoCenterLabelRoom 
            ON InfoCenterTagRoom.tagLabelKey = InfoCenterLabelRoom.infoCenterLabelId
        WHERE InfoCenterEntryRoom.timestamp <= :now
        ORDER BY InfoCenterEntryRoom.timestamp DESC
    """,
    )
    fun getPastInfoCenterData(now: Long): Flow<List<InfoCenterMappedEntryWithTagsRoom>>
}
