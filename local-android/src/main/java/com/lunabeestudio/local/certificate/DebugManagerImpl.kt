/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.certificate

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.StatFs
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.lunabeestudio.domain.di.LogsDir
import com.lunabeestudio.domain.manager.DebugManager
import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.local.crypto.LocalCryptoManager
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.inject.Inject

class DebugManagerImpl @Inject constructor(
    @ApplicationContext appContext: Context,
    private val certificateRoomDao: CertificateRoomDao,
    @LogsDir val logsDir: File,
) : DebugManager {

    private val cryptoPrefs = appContext.getSharedPreferences(LocalCryptoManager.SHARED_PREF_NAME, Context.MODE_PRIVATE)
    private val appPrefs = PreferenceManager.getDefaultSharedPreferences(appContext)
    private val dateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US)

    private var debugFile: File = File(logsDir, appPrefs.currentLogFileName).also { file ->
        logsDir.mkdir()
        file.createNewFile()
        val newSessionBlock = StringBuilder().apply {
            appendLine("++++ Start session  ${dateTimeFormat.format(Date())} ++++")
            appendCommonData(file)
        }.toString()
        file.appendTextAndRotate(newSessionBlock)
    }

    private fun StringBuilder.appendCommonData(file: File) {
        appendLine("${Build.MODEL} - API ${Build.VERSION.SDK_INT}")
        appendLine("Available space = ${StatFs(file.path).availableBytes / 1024 / 1024}mB")
        appendCertificateNoDecrypt()
        appendLine()
    }

    private fun StringBuilder.log() {
        if (!debugFile.exists()) {
            debugFile.apply {
                createNewFile()
                val stringBuilder = StringBuilder()
                stringBuilder.appendLine("++++ Restart session  ${dateTimeFormat.format(Date())} ++++")
                stringBuilder.appendCommonData(this)
                debugFile.appendTextAndRotate(stringBuilder.toString())
            }
        }

        insert(0, "[EVENT]\nDate = ${dateTimeFormat.format(Date())}\n")
        appendLine("[DATA]")
        appendCertificateNoDecrypt()
        appendLine("app pref = ${appPrefs.all}")
        appendLine("crypto pref = ${cryptoPrefs.all}")
        appendLine("Available space = ${StatFs(debugFile.path).availableBytes / 1024 / 1024}mB")
        appendLine()
        debugFile.appendTextAndRotate(this.toString())
    }

    private fun StringBuilder.appendCertificateNoDecrypt() {
        runBlocking {
            withContext(Dispatchers.IO) {
                appendLine(
                    "certificates nodecrypt = ${certificateRoomDao.getAll().joinToString { it.uid }}",
                )
            }
        }
    }

    override fun logSaveCertificates(rawWalletCertificate: RawWalletCertificate, info: String?) {
        StringBuilder().apply {
            appendLine("• Save certificate ${rawWalletCertificate.id} (${rawWalletCertificate.type})")
            if (info != null) {
                appendLine("info = $info")
            }
            log()
        }
    }

    override fun logDeleteCertificates(rawWalletCertificate: RawWalletCertificate, info: String?) {
        StringBuilder().apply {
            appendLine("• Delete certificate ${rawWalletCertificate.id} (${rawWalletCertificate.type})")
            if (info != null) {
                appendLine("info = $info")
            }
            log()
        }
    }

    override fun logObserveCertificate(rawWalletCertificateResult: TacResult<List<RawWalletCertificate>>, info: String?) {
        StringBuilder().apply {
            appendLine("• Observe certificate")
            appendCertificatesResult(rawWalletCertificateResult)

            if (info != null) {
                appendLine("info = $info")
            }
            log()
        }
    }

    override fun logOpenWalletContainer(rawWalletCertificateResult: TacResult<List<RawWalletCertificate>>, info: String?) {
        StringBuilder().apply {
            appendLine("• Open container")
            appendCertificatesResult(rawWalletCertificateResult)
            if (info != null) {
                appendLine("info = $info")
            }
            log()
        }
    }

    private fun StringBuilder.appendCertificatesResult(
        rawWalletCertificateResult: TacResult<List<RawWalletCertificate>>,
    ) {
        appendLine("result = ${rawWalletCertificateResult.print()}")
        if (rawWalletCertificateResult is TacResult.Failure) {
            val error = rawWalletCertificateResult.throwable
            appendLine("error = ${error?.javaClass?.simpleName} ${error?.message}")
        }
    }

    override fun logReinitializeWallet() {
        StringBuilder().apply {
            appendLine("• Reinitialize container")
            log()
        }
    }

    private fun File.appendTextAndRotate(string: String) {
        appendText(string)
        val fileSizeInMb = length().toDouble() / 1024 / 1024
        if (fileSizeInMb > 2) {
            appPrefs.currentLogFileName = when (appPrefs.currentLogFileName) {
                LOGS_FILENAME.first() -> LOGS_FILENAME.last()
                else -> LOGS_FILENAME.first()
            }
        }
    }

    private var SharedPreferences.currentLogFileName: String
        get() = getString(CURRENT_LOG_FILENAME, null) ?: LOGS_FILENAME.first()
        set(value) {
            edit { putString(CURRENT_LOG_FILENAME, value) }
            debugFile = File(logsDir, appPrefs.currentLogFileName)
        }

    private fun TacResult<List<RawWalletCertificate>>.print(): String =
        """${this.javaClass.simpleName} : ${data?.joinToString { "${it.id} (${it.type})" }}"""

    override suspend fun zip(files: List<File>, toZipFile: File) {
        @Suppress("BlockingMethodInNonBlockingContext")
        withContext(Dispatchers.IO) {
            ZipOutputStream(BufferedOutputStream(FileOutputStream(toZipFile.path))).use { out ->
                for (file in files) {
                    FileInputStream(file).use { fi ->
                        BufferedInputStream(fi).use { origin ->
                            val entry = ZipEntry(file.name)
                            out.putNextEntry(entry)
                            origin.copyTo(out, 1024)
                        }
                    }
                }
            }
        }
    }

    companion object {
        private const val CURRENT_LOG_FILENAME: String = "currentLogFilename"
        private val LOGS_FILENAME: List<String> = listOf("event_logs_0.log", "event_logs_1.log")
    }
}
