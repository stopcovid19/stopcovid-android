/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.certificate

import com.google.gson.Gson
import com.lunabeestudio.domain.di.IODispatcher
import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.local.crypto.LocalCryptoManager
import com.lunabeestudio.repository.certificate.CertificateLocalDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class CertificateLocalDataSourceImpl @Inject constructor(
    private val cryptoManager: LocalCryptoManager,
    private val certificateRoomDao: CertificateRoomDao,
    @IODispatcher private val ioDispatcher: CoroutineDispatcher,
) : CertificateLocalDataSource {

    private val gson: Gson = Gson()

    override fun getCertificateByIdFlow(id: String): Flow<RawWalletCertificate?> {
        return certificateRoomDao.getByIdFlow(id).map { certificateRoom ->
            withContext(ioDispatcher) {
                certificateRoom?.let {
                    val decryptedString = cryptoManager.decryptToString(certificateRoom.encryptedValue)
                    gson.fromJson(decryptedString, RawWalletCertificate::class.java)
                }
            }
        }
    }

    override suspend fun getCertificateById(id: String): RawWalletCertificate? {
        return withContext(ioDispatcher) {
            certificateRoomDao.getById(id)?.let { certificateRoom ->
                val decryptedString = cryptoManager.decryptToString(certificateRoom.encryptedValue)
                gson.fromJson(decryptedString, RawWalletCertificate::class.java)
            }
        }
    }

    override val rawWalletCertificatesFlow: Flow<TacResult<List<RawWalletCertificate>>>
        get() = certificateRoomDao.getAllFlow().map { certificatesRoom ->
            withContext(ioDispatcher) {
                var lastError: Exception? = null
                val rawWalletCertificates = certificatesRoom.mapNotNull { (_, encryptedValue) ->
                    try {
                        val decryptedString = cryptoManager.decryptToString(encryptedValue)
                        gson.fromJson(decryptedString, RawWalletCertificate::class.java)
                    } catch (e: Exception) {
                        Timber.e(e)
                        lastError = e
                        null
                    }
                }

                if (lastError == null) {
                    TacResult.Success(rawWalletCertificates)
                } else {
                    TacResult.Failure(lastError, rawWalletCertificates)
                }
            }
        }

    override suspend fun insertAllRawWalletCertificates(certificates: List<RawWalletCertificate>) {
        withContext(ioDispatcher) {
            certificates.map { certificate ->
                val encryptedString = cryptoManager.encryptToString(gson.toJson(certificate))
                CertificateRoom(certificate.id, encryptedString)
            }.let(certificateRoomDao::insertAll)
        }
    }

    override suspend fun updateAllRawWalletCertificates(certificates: List<RawWalletCertificate>) {
        withContext(ioDispatcher) {
            val roomCertificates = certificates.map { certificate ->
                val encryptedString = cryptoManager.encryptToString(gson.toJson(certificate))
                CertificateRoom(certificate.id, encryptedString)
            }

            certificateRoomDao.updateAll(roomCertificates)
        }
    }

    override suspend fun deleteRawWalletCertificate(certificateId: String) {
        withContext(ioDispatcher) {
            certificateRoomDao.delete(certificateId)
        }
    }

    override suspend fun deleteAllRawWalletCertificates() {
        withContext(ioDispatcher) {
            certificateRoomDao.deleteAll()
        }
    }

    override suspend fun getCertificateCount(): Int {
        return withContext(ioDispatcher) {
            certificateRoomDao.getAllCount()
        }
    }

    override val certificateCountFlow: Flow<Int>
        get() = certificateRoomDao.getAllCountFlow()

    override suspend fun forceRefreshCertificatesFlow() {
        val dao = certificateRoomDao
        withContext(ioDispatcher) {
            dao.updateFirstCertificateUid(uid = UUID.randomUUID().toString())
        }
    }

    override suspend fun deleteLostCertificates() {
        val certificateRoomDao = certificateRoomDao
        withContext(ioDispatcher) {
            val certificates = certificateRoomDao.getAll()
            val certificatesToDelete = certificates.filter {
                kotlin.runCatching { cryptoManager.decryptToString(it.encryptedValue) }.isFailure
            }
            certificateRoomDao.delete(certificatesToDelete)
        }
    }

    override fun resetKeyGeneratedFlag() {
        cryptoManager.resetKeyGeneratedFlag()
    }
}
