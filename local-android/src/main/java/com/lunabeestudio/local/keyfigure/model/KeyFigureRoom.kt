/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class KeyFigureRoom(
    @PrimaryKey val labelKey: String,
    val index: Int,
    val category: String,
    val valueGlobalToDisplay: String,
    val valueGlobal: Double?,
    val isFeatured: Boolean,
    val isHighlighted: Boolean?,
    val extractDateS: Long,
    val displayOnSameChart: Boolean,
    val limitLine: Double?,
    val chartType: KeyFigureChartTypeRoom,
    val magnitude: Int,
    val figureType: FigureTypeRoom?,
    @Ignore val series: List<KeyFigureSeriesItemRoom>?,
    @Ignore val avgSeries: List<KeyFigureSeriesItemRoom>?,
) {
    constructor(
        labelKey: String,
        index: Int,
        category: String,
        valueGlobalToDisplay: String,
        valueGlobal: Double?,
        isFeatured: Boolean,
        isHighlighted: Boolean?,
        extractDateS: Long,
        displayOnSameChart: Boolean,
        limitLine: Double?,
        chartType: KeyFigureChartTypeRoom,
        magnitude: Int,
        figureType: FigureTypeRoom?,
    ) : this(
        labelKey, index, category, valueGlobalToDisplay, valueGlobal, isFeatured, isHighlighted, extractDateS,
        displayOnSameChart, limitLine, chartType, magnitude, figureType, listOf(), listOf(),
    )
}

enum class KeyFigureChartTypeRoom {
    BARS,
    LINES,
}

enum class KeyFigureSeriesTypeRoom {
    NAT,
    AVG,
    DEPARTMENT,
}

enum class FigureTypeRoom {
    R,
    PERC,
    PERCK,
    FLOAT,
    INT,
}
