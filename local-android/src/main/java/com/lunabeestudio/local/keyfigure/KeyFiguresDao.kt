/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureAndSeriesRoom
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureAndSeriesAndFavoriteRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureFavoriteRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesItemRoom
import kotlinx.coroutines.flow.Flow

@Dao
interface KeyFiguresDao {

    @Transaction
    @Query("SELECT * FROM KeyFigureRoom ORDER BY `index`")
    fun getAllKeyFigureAndFavoriteFlow(): Flow<List<KeyFigureAndSeriesAndFavoriteRoom>>

    @Transaction
    @Query("SELECT * FROM KeyFigureRoom WHERE labelKey=:label ")
    fun getFlowKeyFigureAndFavoritesByLabel(label: String): Flow<KeyFigureAndSeriesAndFavoriteRoom?>

    @Transaction
    @Query("SELECT * FROM keyfigureroom WHERE labelKey=:label ")
    fun getKeyFiguresAndFavoriteByLabel(label: String): KeyFigureAndSeriesAndFavoriteRoom?

    @Transaction
    @Query("SELECT * FROM KeyFigureRoom ORDER BY `index`")
    fun getAllKeyFiguresAndFavorite(): List<KeyFigureAndSeriesAndFavoriteRoom>

    @Query("SELECT * FROM DepartmentKeyFigureRoom WHERE labelKeyFigure=:label AND dptNb=:dptNb")
    fun getDepartmentByKeyFigureAndNumber(label: String, dptNb: String): DepartmentKeyFigureAndSeriesRoom?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeyFigure(keyFigureRoom: KeyFigureRoom): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDepartment(departmentKeyFigureRoom: DepartmentKeyFigureRoom): Long

    @Insert
    fun insertSeries(series: List<KeyFigureSeriesItemRoom>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavorite(keyFigureFavoriteRoom: KeyFigureFavoriteRoom)

    @Delete
    fun deleteKeyFigures(keyFigureRoom: List<KeyFigureRoom>)

    @Query("DELETE FROM KeyFigureSeriesItemRoom WHERE labelKeyFigure=:label")
    fun deleteSeriesFromKeyFigure(label: String)

    @Query("DELETE FROM KeyFigureFavoriteRoom")
    fun deleteAllFavorites()

    @Transaction
    fun insertNewAndDeleteOld(
        old: List<KeyFigureRoom>,
        new: List<KeyFigureRoom>,
        departments: List<DepartmentKeyFigureRoom>,
    ) {
        deleteKeyFigures(old)
        insertKeyFiguresAndSeries(new)
        insertDepartmentAndSeries(departments)
    }

    @Transaction
    private fun insertKeyFiguresAndSeries(keyFigures: List<KeyFigureRoom>) {
        keyFigures.forEach {
            insertKeyFigure(it)
            deleteSeriesFromKeyFigure(it.labelKey)
            it.avgSeries?.let { series -> insertSeries(series) }
            it.series?.let { series -> insertSeries(series) }
        }
    }

    @Transaction
    private fun insertDepartmentAndSeries(departments: List<DepartmentKeyFigureRoom>) {
        departments.forEach {
            insertDepartment(it)
            it.series?.let { series -> insertSeries(series) }
        }
    }
}
