/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import com.lunabeestudio.local.LocalConstants

@Entity(
    indices = [Index(LocalConstants.RoomColumnName.LabelAndDptNb)],
)
data class DepartmentKeyFigureMapRoom(
    @PrimaryKey val labelAndDptNb: String,
    val labelKey: String,
    val dptNb: String,
    val dptLabel: String,
    @Ignore val series: List<KeyFigureMapSerieItemRoom>?,
) {
    constructor(
        labelKey: String,
        dptNb: String,
        dptLabel: String,
        labelAndDptNb: String,
    ) : this(
        labelKey = labelKey,
        dptNb = dptNb,
        dptLabel = dptLabel,
        labelAndDptNb = labelAndDptNb,
        series = listOf(),
    )
}
