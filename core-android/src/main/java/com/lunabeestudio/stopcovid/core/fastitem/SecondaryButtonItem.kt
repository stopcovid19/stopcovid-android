/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.fastitem

import com.lunabeestudio.stopcovid.core.R

class SecondaryButtonItem : ButtonItem(
    layout = R.layout.item_secondary_button,
    id = R.id.item_secondary_button,
)

fun secondaryButtonItem(block: (SecondaryButtonItem.() -> Unit)): SecondaryButtonItem = SecondaryButtonItem()
    .apply(block)
