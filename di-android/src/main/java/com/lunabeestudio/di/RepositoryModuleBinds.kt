/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import com.lunabeestudio.domain.manager.DebugManager
import com.lunabeestudio.domain.repository.BlacklistRepository
import com.lunabeestudio.domain.repository.CertificateDocumentRepository
import com.lunabeestudio.domain.repository.CertificateRepository
import com.lunabeestudio.domain.repository.DccCertificateRepository
import com.lunabeestudio.domain.repository.EUTagsRepository
import com.lunabeestudio.domain.repository.FeaturedInfoRepository
import com.lunabeestudio.domain.repository.InfoCenterRepository
import com.lunabeestudio.domain.repository.KeyFigureMapRepository
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.domain.repository.RisksLevelRepository
import com.lunabeestudio.domain.repository.SmartWalletEligibilityRepository
import com.lunabeestudio.domain.repository.SmartWalletValidityRepository
import com.lunabeestudio.local.certificate.DebugManagerImpl
import com.lunabeestudio.repository.blacklist.BlacklistRepositoryImpl
import com.lunabeestudio.repository.certificate.CertificateDocumentRepositoryImpl
import com.lunabeestudio.repository.certificate.CertificateRepositoryImpl
import com.lunabeestudio.repository.certificate.DccCertificateRepositoryImpl
import com.lunabeestudio.repository.eutags.EUTagsRepositoryImpl
import com.lunabeestudio.repository.featuredinfo.FeaturedInfoRepositoryImpl
import com.lunabeestudio.repository.infocenter.InfoCenterRepositoryImpl
import com.lunabeestudio.repository.keyfigure.KeyFigureMapRepositoryImpl
import com.lunabeestudio.repository.keyfigure.KeyFigureRepositoryImpl
import com.lunabeestudio.repository.risk.RisksLevelRepositoryImpl
import com.lunabeestudio.repository.smartwallet.SmartWalletEligibilityRepositoryImpl
import com.lunabeestudio.repository.smartwallet.SmartWalletValidityRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModuleBinds {

    @Binds
    abstract fun bindDebugManager(
        debugManagerImpl: DebugManagerImpl,
    ): DebugManager

    @Binds
    abstract fun bindBlacklistRepository(
        blacklistRepositoryImpl: BlacklistRepositoryImpl,
    ): BlacklistRepository

    @Binds
    abstract fun bindCertificateRepository(
        certificateRepositoryImpl: CertificateRepositoryImpl,
    ): CertificateRepository

    @Binds
    abstract fun bindKeyFigureRepository(
        keyFigureRepositoryImpl: KeyFigureRepositoryImpl,
    ): KeyFigureRepository

    @Binds
    abstract fun bindKeyFigureMapRepository(
        keyFigureMapRepositoryImpl: KeyFigureMapRepositoryImpl,
    ): KeyFigureMapRepository

    @Binds
    abstract fun bindInfoCenterRepository(
        infoCenterRepositoryImpl: InfoCenterRepositoryImpl,
    ): InfoCenterRepository

    @Binds
    abstract fun bindFeaturedInfoRepository(
        featuredInfoRepositoryImpl: FeaturedInfoRepositoryImpl,
    ): FeaturedInfoRepository

    @Binds
    abstract fun bindCertificateDocumentRepository(
        certificateDocumentRepositoryImpl: CertificateDocumentRepositoryImpl,
    ): CertificateDocumentRepository

    @Binds
    abstract fun bindDccCertificateRepository(
        dccCertificateRepositoryImpl: DccCertificateRepositoryImpl,
    ): DccCertificateRepository

    @Binds
    abstract fun bindRisksLevelRepository(
        risksLevelRepositoryImpl: RisksLevelRepositoryImpl,
    ): RisksLevelRepository

    @Binds
    abstract fun bindEUTagsRepository(
        euTagsRepositoryImpl: EUTagsRepositoryImpl,
    ): EUTagsRepository

    @Binds
    abstract fun bindSmartWalletEligibilityRepository(
        smartWalletEligibilityRepositoryImpl: SmartWalletEligibilityRepositoryImpl,
    ): SmartWalletEligibilityRepository

    @Binds
    abstract fun bindSmartWalletValidityRepository(
        smartWalletValidityRepositoryImpl: SmartWalletValidityRepositoryImpl,
    ): SmartWalletValidityRepository
}
