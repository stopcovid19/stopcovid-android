/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.lunabeestudio.local.AppDatabase
import com.lunabeestudio.local.blacklist.BlacklistPrefixDao
import com.lunabeestudio.local.certificate.CertificateRoomDao
import com.lunabeestudio.local.featuredinfo.FeaturedInfoDao
import com.lunabeestudio.local.info.InfoCenterDao
import com.lunabeestudio.local.keyfigure.KeyFiguresDao
import com.lunabeestudio.local.keyfigure.KeyFiguresMapDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocalModuleProvides {

    @Provides
    fun provideSharedPrefs(
        @ApplicationContext context: Context,
    ): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun provideAppDatabase(
        @ApplicationContext context: Context,
    ): AppDatabase {
        return AppDatabase.build(context)
    }

    @Provides
    fun provideBlacklistPrefixDao(
        appDatabase: AppDatabase,
    ): BlacklistPrefixDao = appDatabase.blacklistPrefixDao()

    @Provides
    fun provideCertificateRoomDao(
        appDatabase: AppDatabase,
    ): CertificateRoomDao = appDatabase.certificateRoomDao()

    @Provides
    fun provideKeyFiguresDao(
        appDatabase: AppDatabase,
    ): KeyFiguresDao = appDatabase.keyFiguresDao()

    @Provides
    fun provideKeyFiguresMapDao(
        appDatabase: AppDatabase,
    ): KeyFiguresMapDao = appDatabase.keyFiguresMapDao()

    @Provides
    fun provideInfoCenterDao(
        appDatabase: AppDatabase,
    ): InfoCenterDao = appDatabase.infoCenterDao()

    @Provides
    fun provideFeaturedInfoDao(
        appDatabase: AppDatabase,
    ): FeaturedInfoDao = appDatabase.featuredInfoDao()
}
