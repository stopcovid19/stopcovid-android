/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import com.lunabeestudio.domain.utils.JCEUtils
import com.lunabeestudio.local.crypto.JCEUtilsImpl
import com.lunabeestudio.remote.server.FileRemoteToLocalDataSourceImpl
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DomainModuleBinds {

    @Binds
    abstract fun bindJCEUtils(
        jceUtilsImpl: JCEUtilsImpl,
    ): JCEUtils

    @Binds
    abstract fun bindRemoteToLocalFileDataSource(
        remoteToLocalFileDataSourceImpl: FileRemoteToLocalDataSourceImpl,
    ): FileRemoteToLocalDataSource
}
