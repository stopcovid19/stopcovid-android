# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-dontobfuscate
-keep public class com.lunabeestudio.** { *; }
-keep public class dgca.verifier.app.decoder.model.** { *; }

# Protobuf https://github.com/protocolbuffers/protobuf/issues/6463#issuecomment-632884075
-keep class * extends com.google.protobuf.GeneratedMessageLite { *; }

# Bouncy Castle -- Keep AES, ECDH & BCKS
-keep class org.bouncycastle.** { *; }
#-keep class org.bouncycastle.jcajce.provider.symmetric.AES$*
#-keep class org.bouncycastle.jcajce.provider.asymmetric.EC$* { *; }
#-keep class org.bouncycastle.jcajce.provider.asymmetric.ec.KeyPairGeneratorSpi$ECDH { *; }
#-keep class org.bouncycastle.jcajce.provider.asymmetric.ec.KeyFactorySpi$ECDH { *; }
#-keep class org.bouncycastle.jcajce.provider.asymmetric.ec.KeyFactorySpi$ECDSA { *; }
#-keep class org.bouncycastle.jcajce.provider.keystore.BC$* { *; }
#-keep class org.bouncycastle.jcajce.provider.keystore.bc.BcKeyStoreSpi$* { *; }
#-keep class org.bouncycastle.jcajce.provider.keystore.bcfks.BcFKSKeyStoreSpi$* { *; }

# json-schema-validator
-keep class com.github.fge.jsonschema.** { *; }
-keep class org.mozilla.javascript.** { *; }

# Generated rules
-dontwarn com.gemalto.jp2.JP2Decoder
-dontwarn com.gemalto.jp2.JP2Encoder
-dontwarn javax.imageio.spi.ImageInputStreamSpi
-dontwarn javax.imageio.spi.ImageOutputStreamSpi
-dontwarn javax.imageio.spi.ImageReaderSpi
-dontwarn javax.imageio.spi.ImageWriterSpi
-dontwarn javax.naming.Binding
-dontwarn javax.naming.NamingEnumeration
-dontwarn javax.naming.NamingException
-dontwarn javax.naming.directory.Attribute
-dontwarn javax.naming.directory.Attributes
-dontwarn javax.naming.directory.DirContext
-dontwarn javax.naming.directory.InitialDirContext
-dontwarn javax.naming.directory.SearchControls
-dontwarn javax.naming.directory.SearchResult
-dontwarn javax.script.Invocable
-dontwarn javax.script.ScriptEngine
-dontwarn javax.script.ScriptEngineManager
-dontwarn javax.script.ScriptException
-dontwarn org.bouncycastle.jsse.BCSSLParameters
-dontwarn org.bouncycastle.jsse.BCSSLSocket
-dontwarn org.bouncycastle.jsse.provider.BouncyCastleJsseProvider
-dontwarn org.conscrypt.Conscrypt$Version
-dontwarn org.conscrypt.Conscrypt
-dontwarn org.conscrypt.ConscryptHostnameVerifier
-dontwarn org.joda.convert.FromString
-dontwarn org.joda.convert.ToString
-dontwarn org.mozilla.javascript.Context
-dontwarn org.mozilla.javascript.Function
-dontwarn org.mozilla.javascript.Scriptable
-dontwarn org.mozilla.javascript.ScriptableObject
-dontwarn org.openjsse.javax.net.ssl.SSLParameters
-dontwarn org.openjsse.javax.net.ssl.SSLSocket
-dontwarn org.openjsse.net.ssl.OpenJSSE