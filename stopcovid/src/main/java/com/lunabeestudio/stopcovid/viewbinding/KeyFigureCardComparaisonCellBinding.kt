/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/17 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewbinding

import androidx.annotation.ColorInt
import androidx.core.view.isVisible
import com.lunabeestudio.stopcovid.core.UiConstants
import com.lunabeestudio.stopcovid.core.extension.isNightMode
import com.lunabeestudio.stopcovid.core.extension.setOnClickListenerOrHideRipple
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.databinding.ItemKeyFigureCardValueRowBinding
import com.lunabeestudio.stopcovid.databinding.ItemKeyFigureComparisonCardBinding
import com.lunabeestudio.stopcovid.extension.colorStringKeyFromKeyFigureLabel
import com.lunabeestudio.stopcovid.extension.formatNumberIfNeeded
import com.lunabeestudio.stopcovid.extension.safeParseColor
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonData
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonRowData
import java.text.DateFormat
import java.text.NumberFormat

fun ItemKeyFigureComparisonCardBinding.bindToKeyFigureData(
    data: KeyFigureComparisonData,
    strings: LocalizedStrings,
    dateFormat: DateFormat,
    onClickDetailMin: () -> Unit,
    onClickDetailMax: () -> Unit,
    numberFormat: NumberFormat,
) {
    val isDarkMode = root.context.isNightMode()
    if (data.descriptionData != null) {
        this.descriptionTextView.isVisible = true
        this.descriptionTextView.text = data.descriptionData.formatString(strings, numberFormat)
    } else {
        this.descriptionTextView.isVisible = false
    }
    if (data.keyFigureComparisonDayMinusSeven != null) {
        this.dayMinusSevenRow.rowLayout.isVisible = true
        this.dayMinusSevenRow.bindToKeyFigureComparisonRowData(
            data.keyFigureComparisonDayMinusSeven,
            strings,
            dateFormat,
            numberFormat,
            null,
            strings[data.labelKeyFigure.colorStringKeyFromKeyFigureLabel(isDarkMode)].safeParseColor(),
        )
    } else {
        this.dayMinusSevenRow.rowLayout.isVisible = false
    }
    this.maxValueRow.bindToKeyFigureComparisonRowData(
        data.keyFigureComparisonMaxValue,
        strings,
        dateFormat,
        numberFormat,
        onClickDetailMax,
        UiConstants.KeyFigure.COLOR_MAX_COMPARISON.getColorFromTheme(isDarkMode).safeParseColor(),
    )
    this.minValueRow.bindToKeyFigureComparisonRowData(
        data.keyFigureComparisonMinValue,
        strings,
        dateFormat,
        numberFormat,
        onClickDetailMin,
        UiConstants.KeyFigure.COLOR_MIN_COMPARISON.getColorFromTheme(isDarkMode).safeParseColor(),
    )
}

fun ItemKeyFigureCardValueRowBinding.bindToKeyFigureComparisonRowData(
    data: KeyFigureComparisonRowData,
    strings: LocalizedStrings,
    dateFormat: DateFormat,
    numberFormat: NumberFormat,
    onClickDetail: (() -> Unit)?,
    @ColorInt color: Int,
) {
    val dateText = when {
        data.date.size <= 2 -> {
            data.date.joinToString(", ") {
                dateFormat.format(it)
            }
        }
        else -> {
            this.root.setOnClickListenerOrHideRipple { onClickDetail?.invoke() }
            strings["keyFigureDetailController.section.comparison.dateOccurrences"]?.format(
                dateFormat.format(data.date.last()),
                data.date.size - 1,
            )
        }
    }
    this.dateTextView.text = dateText
    this.labelTextView.text = strings[data.labelRes]
    this.valueTextView.text = data.value.formatNumberIfNeeded(numberFormat)
    this.valueTextView.setTextColor(color)
}
