/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fastitem

import android.text.Spanned
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.databinding.ItemMapFranceBinding
import com.lunabeestudio.stopcovid.view.map.view.Region
import com.lunabeestudio.stopcovid.view.map.view.marker.KeyFigureMarkerView
import com.mikepenz.fastadapter.binding.AbstractBindingItem

enum class PayLoadMapType {
    PAYLOAD_PROGRESS,
    PAYLOAD_STATE,
}

data class PayLoadMap(
    val payLoadType: PayLoadMapType,
    val data: List<Any>,
)

enum class StatePlay {
    PLAY, PAUSE;

    fun getDrawable(): Int = if (this == PLAY) R.drawable.ic_baseline_pause else R.drawable.ic_baseline_play_arrow
    fun toggle(): StatePlay = if (this == PLAY) PAUSE else PLAY
}

class MapFranceItem : AbstractBindingItem<ItemMapFranceBinding>() {

    override val type: Int = R.id.map_france_item

    var maxSeekBarValue: Int? = null
    var regions: List<Region>? = null
    var statePlayInit: StatePlay? = null
    var initialProgress: Int = 0
    var dataNotAvailableText: String? = null
    var onProgressSeekBarChanged: ((Int) -> Unit)? = null
    var onPlayPauseClicked: (() -> Unit)? = null
    var getMarkerView: (() -> KeyFigureMarkerView)? = null
    var onShareCard: ((binding: ItemMapFranceBinding) -> Unit)? = null
    var keyFigureName: String? = null
    var contentDescription: String? = null
    var contentDescriptionShare: String? = null
    var getMapDescription: (() -> Spanned)? = null

    @ColorRes
    var noDataRegionColor: Int? = null

    @ColorRes
    var noDataRegionSelectedColor: Int? = null

    @DrawableRes
    var drawableMap: Int = R.drawable.ic_france_departments

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemMapFranceBinding {
        return ItemMapFranceBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemMapFranceBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        val payload = if (payloads.isNotEmpty()) payloads[0] as PayLoadMap else null

        if (payload == null) {
            binding.apply {
                maxSeekBarValue?.let {
                    seekBar.max = it
                }
                map.contentDescription = contentDescription
                shareButton.contentDescription = contentDescriptionShare
                binding.seekBar.progress = initialProgress
                regions?.let {
                    map.regions = it
                    map.updateMap()
                    dateTextView.text = it.firstOrNull()?.date
                }
                statePlayInit?.let {
                    playButton.setImageResource(it.getDrawable())
                }
                indicatorNameForShareTextView.text = keyFigureName
                shareButton.setOnClickListener { onShareCard?.invoke(this) }
                map.getMarkerView = getMarkerView
                map.textNoData = dataNotAvailableText
                map.mapDrawable = drawableMap
                map.resetMap()
                playButton.setOnClickListener {
                    onPlayPauseClicked?.invoke()
                }
                descriptionTextView.setTextOrHide(getMapDescription?.invoke())
                setupSeekBar(seekBar)
                noDataRegionColor?.let {
                    map.defaultRegionBackgroundColor = ContextCompat.getColor(this.root.context, it)
                }
                noDataRegionSelectedColor?.let {
                    map.defaultRegionSelectedColor = ContextCompat.getColor(this.root.context, it)
                }
            }
        }
        payload?.let {
            @Suppress("UNCHECKED_CAST")
            when (it.payLoadType) {
                PayLoadMapType.PAYLOAD_STATE -> changeState(binding, it.data[0] as StatePlay)
                PayLoadMapType.PAYLOAD_PROGRESS -> changeProgress(binding, it.data[0] as Int, it.data[1] as List<Region>)
            }
        }
    }

    private fun changeProgress(binding: ItemMapFranceBinding, progress: Int, regions: List<Region>) {
        binding.map.regions = regions
        binding.descriptionTextView.setTextOrHide(getMapDescription?.invoke())
        binding.map.updateMap()
        binding.seekBar.progress = progress
        binding.dateTextView.text = regions.firstOrNull()?.date
    }

    private fun changeState(binding: ItemMapFranceBinding, statePlay: StatePlay) {
        binding.playButton.setImageResource(statePlay.getDrawable())
    }

    private fun setupSeekBar(seekbar: SeekBar) {
        seekbar.setOnSeekBarChangeListener(
            object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        onProgressSeekBarChanged?.invoke(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            },
        )
    }
}

fun mapFranceItem(block: (MapFranceItem.() -> Unit)): MapFranceItem = MapFranceItem().apply(block)
