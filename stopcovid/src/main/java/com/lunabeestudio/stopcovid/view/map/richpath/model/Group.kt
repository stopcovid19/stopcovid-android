/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by tarek360, Modified by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.richpath.model

import android.graphics.Matrix

class Group {
    companion object {
        const val TAG_NAME = "group"
    }

    private var matrix: Matrix? = null

    fun matrix(): Matrix {
        val matrix = this.matrix ?: Matrix()
        this.matrix = Matrix()
        return matrix
    }

    fun scale(matrix: Matrix) {
        matrix().postConcat(matrix)
    }
}
