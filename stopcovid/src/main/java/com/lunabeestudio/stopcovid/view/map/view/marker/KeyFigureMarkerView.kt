/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.view.marker

import android.content.Context
import android.util.AttributeSet
import androidx.cardview.widget.CardView
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import com.lunabeestudio.stopcovid.databinding.MarkerViewMapLayoutBinding
import com.lunabeestudio.stopcovid.view.map.view.Region

class KeyFigureMarkerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0,
) : CardView(context, attrs, defStyleAttr) {

    private val layoutParams = LayoutParams(
        LayoutParams.WRAP_CONTENT,
        LayoutParams.WRAP_CONTENT,
    )

    var binding: MarkerViewMapLayoutBinding? = null

    private val layoutRes: Int = R.layout.marker_view_map_layout

    init {
        setLayoutParams(layoutParams)
        radius = R.dimen.corner_radius.toDimensSize(context)
        elevation = R.dimen.cardview_default_elevation.toDimensSize(context)
        inflate(context, layoutRes, this)
    }

    fun bindView(region: Region) {
        binding = MarkerViewMapLayoutBinding.bind(this.getChildAt(0))
        binding?.regionNameTextView?.setTextOrHide(region.name)
        binding?.valueRegionTextView?.setTextOrHide(region.formattedValue)
    }
}
