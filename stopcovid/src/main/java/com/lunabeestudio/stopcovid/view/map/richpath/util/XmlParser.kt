/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by tarek360, Modified by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.richpath.util

import android.content.Context
import android.content.res.XmlResourceParser
import androidx.core.content.ContextCompat
import com.lunabeestudio.stopcovid.view.map.richpath.RichPath
import com.lunabeestudio.stopcovid.view.map.richpath.model.Group
import com.lunabeestudio.stopcovid.view.map.richpath.model.Vector
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.Stack

object XmlParser {
    private const val NAMESPACE = "http://schemas.android.com/apk/res/android"

    @Throws(IOException::class, XmlPullParserException::class)
    fun parseVector(vector: Vector, xpp: XmlResourceParser, context: Context) {
        val groupStack = Stack<Group>()
        var eventType = xpp.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            val tagName = xpp.name
            if (eventType == XmlPullParser.START_TAG) {
                when (tagName) {
                    Vector.TAG_NAME -> parseVectorElement(vector, xpp, context)
                    Group.TAG_NAME -> {
                        val group = parseGroupElement()
                        if (!groupStack.empty()) {
                            group.scale(groupStack.peek().matrix())
                        }
                        groupStack.push(group)
                    }
                    RichPath.TAG_NAME -> {
                        parsePathElement(context, xpp)?.run {
                            if (!groupStack.empty()) {
                                applyGroup(groupStack.peek())
                            }
                            vector.paths.add(this)
                        }
                    }
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (Group.TAG_NAME == tagName) {
                    if (!groupStack.empty()) {
                        groupStack.pop()
                    }
                }
            }
            eventType = xpp.next()
        }
        xpp.close()
    }

    private fun parseVectorElement(vector: Vector, xpp: XmlResourceParser, context: Context) {
        vector.inflate(xpp, context)
    }

    private fun parseGroupElement(): Group {
        return Group()
    }

    private fun parsePathElement(context: Context, xpp: XmlResourceParser): RichPath? {
        val pathData = getAttributeString(context, xpp, "pathData", null) ?: return null
        val path = RichPath(pathData)
        path.inflate(context, xpp)
        return path
    }

    fun getAttributeString(
        context: Context,
        xpp: XmlResourceParser,
        attributeName: String,
        defValue: String?,
    ): String? {
        val resourceId = getAttributeResourceValue(xpp, attributeName)
        val value: String? = if (resourceId != -1) {
            context.getString(resourceId)
        } else {
            getAttributeValue(xpp, attributeName)
        }
        return value ?: defValue
    }

    fun getAttributeFloat(xpp: XmlResourceParser, attributeName: String, defValue: Float): Float {
        return getAttributeValue(xpp, attributeName)?.toFloat() ?: defValue
    }

    fun getAttributeDimen(
        context: Context,
        xpp: XmlResourceParser,
        attributeName: String,
        defValue: Float,
    ): Float {
        val value = getAttributeValue(xpp, attributeName) ?: return defValue
        return Utils.dpToPixel(context, Utils.getDimenFromString(value))
    }

    fun getAttributeColor(
        context: Context,
        xpp: XmlResourceParser,
        attributeName: String,
        defValue: Int,
    ): Int {
        val resourceId = getAttributeResourceValue(xpp, attributeName)
        if (resourceId != -1) {
            return ContextCompat.getColor(context, resourceId)
        }
        return getAttributeValue(xpp, attributeName)?.let { Utils.getColorFromString(it) }
            ?: defValue
    }

    private fun getAttributeValue(xpp: XmlResourceParser, attributeName: String): String? {
        return xpp.getAttributeValue(NAMESPACE, attributeName)
    }

    private fun getAttributeResourceValue(xpp: XmlResourceParser, attributeName: String): Int {
        return xpp.getAttributeResourceValue(NAMESPACE, attributeName, -1)
    }
}
