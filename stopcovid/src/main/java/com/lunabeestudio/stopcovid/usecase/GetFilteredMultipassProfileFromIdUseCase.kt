/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/1/19 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.stopcovid.extension.isFromMultiPassIssuer
import com.lunabeestudio.stopcovid.extension.multipassProfileId
import com.lunabeestudio.stopcovid.extension.testResultIsNegative
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.MultipassProfile
import com.lunabeestudio.stopcovid.repository.WalletRepository
import java.text.DateFormat
import kotlin.time.Duration.Companion.milliseconds

class GetFilteredMultipassProfileFromIdUseCase(
    private val configurationManager: ConfigurationManager,
    private val walletRepository: WalletRepository,
    private val dateFormat: DateFormat,
) {
    operator fun invoke(profileId: String): MultipassProfile? {
        val multipassConfig = configurationManager.configuration.multipassConfig ?: return null
        var certificateInfo: EuropeanCertificate? = null
        val certificates = walletRepository.walletCertificateFlow.value.data.orEmpty()
            .asSequence()
            .filterIsInstance<EuropeanCertificate>()
            .filter { dcc -> dcc.multipassProfileId() == profileId }
            .filterNot { dcc ->
                certificateInfo = dcc // Extract info asap in case no certificate match
                dcc.type == WalletCertificateType.MULTI_PASS
            }
            .filterNot { dcc ->
                dcc.isFromMultiPassIssuer()
            }
            .filterNot { dcc ->
                dcc.greenCertificate.testResultIsNegative == true &&
                    (System.currentTimeMillis() - dcc.timestamp).milliseconds > multipassConfig.testMaxDuration
            }
            .distinctBy { dcc -> dcc.sha256 }
            .sortedByDescending { it.timestamp }
            .toList()
            .filterNot { dcc -> dcc.isBlacklisted == true }
        return MultipassProfile(profileId, certificateInfo, certificates, dateFormat)
    }
}
