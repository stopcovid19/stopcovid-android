/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/6/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.repository.EUTagsRepository
import com.lunabeestudio.stopcovid.extension.isAntigen
import com.lunabeestudio.stopcovid.extension.manufacturer
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate

class IsCertificateEUValidUseCase(
    private val euTagsRepository: EUTagsRepository,
) {
    operator fun invoke(walletCertificate: WalletCertificate): Boolean {
        val isAntigen = (walletCertificate as? EuropeanCertificate)?.greenCertificate?.isAntigen == true
        val isValidatedManufacturer = (walletCertificate as? EuropeanCertificate)?.greenCertificate?.manufacturer?.let { manufacturer ->
            euTagsRepository.isEUTag(manufacturer)
        } ?: false
        return !isAntigen || isValidatedManufacturer
    }
}
