/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.SharedPreferences
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.domain.repository.KeyFigureRepository

class DeletePostalCodeUseCase(
    private val sharedPrefs: SharedPreferences,
    private val keyFiguresRepository: KeyFigureRepository,
) {
    suspend operator fun invoke() {
        sharedPrefs.chosenPostalCode = null
        keyFiguresRepository.onAppForeground()
    }
}
