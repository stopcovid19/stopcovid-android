/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/3 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.core.content.edit
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.extension.lastVersionMigration
import timber.log.Timber
import java.io.File

class MigrateAppUseCase(
    val context: Context,
    val sharedPrefs: SharedPreferences,
) {
    private var lastVersion = sharedPrefs.lastVersionMigration

    operator fun invoke() {
        Timber.i("Migrate from v$lastVersion to v$CURRENT_MIGRATION_VERSION")

        if (lastVersion == 0) {
            executeMigrationFromV0toV1()
            lastVersion++
        }
        if (lastVersion == 1) {
            executeMigrationFromV1toV2()
            lastVersion++
        }
        if (lastVersion == 2) {
            // not needed anymore
            lastVersion++
        }
        if (lastVersion == 3) {
            // not needed anymore
            lastVersion++
        }
        if (lastVersion == 4) {
            executeMigrationFromV4toV5()
            lastVersion++
        }

        sharedPrefs.lastVersionMigration = lastVersion
    }

    private fun executeMigrationFromV0toV1() {
        Timber.i("Run blacklist v4 migration")
        sharedPrefs.edit {
            remove("Blacklist.Dcc.Iteration")
            remove("Blacklist.2Ddoc.Iteration")
        }
        context.deleteDatabase("blacklist.db")
    }

    private fun executeMigrationFromV1toV2() {
        Timber.i("Run InfoCenter migration")
        File(context.filesDir, "info-center-lastupdate.json").delete()
        File(context.filesDir, "info-center.json").delete()
        File(context.filesDir, "info-tags.json").delete()
        File(context.filesDir, "info-labels-fr.json").delete()

        sharedPrefs.edit {
            remove(Constants.SharedPrefs.LAST_INFO_CENTER_REFRESH)
            remove("Last.Info.Center.Fetch")
        }
    }

    private fun executeMigrationFromV4toV5() {
        sharedPrefs.edit {
            remove("isVenueOnBoardingDone")
            remove("venuesFeaturedWasActivatedAtLeastOneTime")
            remove("alertRiskLevelChanged")
            remove("hideRiskStatus")
            remove("hasUsedUniversalQrScan")
            remove("Last.Tracing.Flag.Value")
        }
        File(context.filesDir, "epochs").deleteRecursively()
        File(context.filesDir, "local_proximity").deleteRecursively()
        File(context.filesDir, "TacAnalytics").deleteRecursively()
        File(context.filesDir, "form.json").delete()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.deleteSharedPreferences("robert_prefs")
            context.deleteSharedPreferences("TacAnalytics")
        } else {
            context.getSharedPreferences("robert_prefs", Context.MODE_PRIVATE).edit { clear() }
            context.getSharedPreferences("TacAnalytics", Context.MODE_PRIVATE).edit { clear() }
        }
    }

    companion object {
        const val CURRENT_MIGRATION_VERSION: Int = 5
    }
}
