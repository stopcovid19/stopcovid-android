/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import android.content.Context
import androidx.core.app.ShareCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.manager.DebugManager
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.core.utils.SingleLiveEvent
import com.lunabeestudio.stopcovid.extension.listLogFiles
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.usecase.ChangeLanguageUseCase
import kotlinx.coroutines.launch
import java.io.File
import java.util.Locale

class SettingsViewModel(
    private val walletRepository: WalletRepository,
    private val debugManager: DebugManager,
    private val changeLanguageUseCase: ChangeLanguageUseCase,
    private val logsDir: File,
) : ViewModel() {

    val eraseLocalSuccess: SingleLiveEvent<Unit> = SingleLiveEvent()
    val eraseRemoteSuccess: SingleLiveEvent<Unit> = SingleLiveEvent()
    val loadingInProgress: MutableLiveData<Boolean> = MutableLiveData(false)

    fun eraseCertificates() {
        viewModelScope.launch {
            walletRepository.deleteAllCertificates()
            eraseLocalSuccess.postValue(Unit)
        }
    }

    fun exportLogs(context: Context, strings: LocalizedStrings) {
        viewModelScope.launch {
            loadingInProgress.postValue(true)
            val shareFile = File(context.cacheDir, "share_log.zip")
            debugManager.zip(logsDir.listLogFiles() ?: emptyList(), shareFile)
            shareLogs(context, shareFile, strings)
            loadingInProgress.postValue(false)
        }
    }

    private fun shareLogs(context: Context, file: File, strings: LocalizedStrings) {
        val uri = FileProvider.getUriForFile(
            context,
            "${context.packageName}.share",
            file,
        )

        ShareCompat.IntentBuilder(context)
            .setType("message/rfc822")
            .setEmailTo(arrayOf(strings["manageDataController.logs.email"] ?: "contact@tousanticovid.gouv.fr"))
            .setStream(uri)
            .setSubject(strings["manageDataController.logs.subject"] ?: "Partage des fichiers de logs TAC")
            .startChooser()
    }

    fun changeLanguage(locale: Locale): Unit = changeLanguageUseCase(locale)
}

class SettingsViewModelFactory(
    private val walletRepository: WalletRepository,
    private val debugManager: DebugManager,
    private val changeLanguageUseCase: ChangeLanguageUseCase,
    private val logsDir: File,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return SettingsViewModel(
            walletRepository,
            debugManager,
            changeLanguageUseCase,
            logsDir,
        ) as T
    }
}
