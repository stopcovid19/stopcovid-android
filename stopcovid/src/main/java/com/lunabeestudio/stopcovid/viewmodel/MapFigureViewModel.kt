/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.annotation.ColorInt
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.DepartmentKeyFigureMap
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.domain.repository.KeyFigureMapRepository
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.extension.shortUnitStringKey
import com.lunabeestudio.stopcovid.fastitem.StatePlay
import com.lunabeestudio.stopcovid.view.map.view.Region
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.util.Date
import kotlin.math.min

data class RegionUiData(
    @ColorInt val colorRegion: Int = 0,

    @ColorInt val colorSelectedRegion: Int = 0,
)

class MapFigureViewModel(
    private val strings: LocalizedStrings,
    private val keyFigureMapRepository: KeyFigureMapRepository,
    private val keyFigureRepository: KeyFigureRepository,
    var regionUiData: RegionUiData,
    private val toDateFormat: DateFormat,
) : ViewModel() {

    private val _chosenKeyFigureMap: MutableStateFlow<KeyFigureMap?> = MutableStateFlow(null)
    val chosenKeyFigureMap: StateFlow<KeyFigureMap?> = _chosenKeyFigureMap
    var chosenKeyFigure: KeyFigure? = null

    val availableKeyFiguresFlow: Flow<List<String>> = keyFigureMapRepository.availableKeyFiguresFlow()

    var maxProgressSeekBar: Int = 0
    var minValue: Float = 0f
    var maxValue: Float = 0f

    var regionsMap: List<Region> = listOf()

    private val _statePlay = MutableStateFlow(StatePlay.PAUSE)
    val statePlay: StateFlow<StatePlay>
        get() = _statePlay

    private val _progress = MutableStateFlow(0)
    val progress: StateFlow<Int>
        get() = _progress

    init {
        viewModelScope.launch {
            keyFigureMapRepository.fetchNewData()
        }
        setupAutoPlay()
    }

    private fun setupAutoPlay() {
        viewModelScope.launch {
            while (true) {
                delay(Constants.Map.ANIMATION_DURATION_AUTO_PLAY)
                if (statePlay.value == StatePlay.PLAY) {
                    chosenKeyFigureMap.value?.valuesDepartments?.let {
                        val newProgress = if (progress.value == maxProgressSeekBar) 0 else progress.value + 1
                        regionsMap = getRegionsMap(newProgress)
                        _progress.value = newProgress
                    }
                }
            }
        }
    }

    fun setKeyFigureByLabel(labelKey: String?, progress: Int) { // Fallback to default key Figures if saved figure does not exist anymore
        viewModelScope.launch {
            chosenKeyFigure = labelKey?.let {
                keyFigureRepository.keyFigureByLabel(labelKey).data
            }
            val lastFigureSeen = labelKey?.let {
                keyFigureMapRepository.keyFigureMapByLabel(labelKey).data
            }
            val initialProgress = if (lastFigureSeen != null) {
                _chosenKeyFigureMap.value = lastFigureSeen
                progress
            } else {
                _chosenKeyFigureMap.value = keyFigureMapRepository.keyFigureMapByLabel(
                    keyFigureMapRepository.availableKeyFiguresFlow().first().first(),
                ).data
                // If the last key figure doesn't exist anymore, we reset the progress to O
                0
            }
            maxProgressSeekBar = (chosenKeyFigureMap.value?.valuesDepartments?.maxOfOrNull { it.series?.size ?: 0 } ?: 0) - 1
            minValue = chosenKeyFigureMap.value?.valuesDepartments?.minOfOrNull { valueDepartment ->
                valueDepartment.series?.minOfOrNull { serie -> serie.value.toFloat() } ?: 0f
            } ?: 0f
            maxValue = chosenKeyFigureMap.value?.valuesDepartments?.maxOfOrNull { valueDepartment ->
                valueDepartment.series?.maxOfOrNull { serie -> serie.value.toFloat() } ?: 0f
            } ?: 0f

            // Fallback to max progress possible
            val safeInitialProgress = min(initialProgress, maxProgressSeekBar)
            setProgress(safeInitialProgress)
        }
    }

    fun setProgressManually(value: Int) {
        _statePlay.value = StatePlay.PAUSE
        setProgress(value)
    }

    fun setProgress(value: Int) {
        regionsMap = getRegionsMap(value)
        _progress.value = value
    }

    fun invertStatePlay() {
        _statePlay.value = statePlay.value.toggle()
    }

    fun setStatePlay(value: StatePlay) {
        _statePlay.value = value
    }

    private fun getRegionsMap(entrySelected: Int): List<Region> {
        return chosenKeyFigureMap.value?.valuesDepartments?.map {
            val value = it.series?.getOrNull(entrySelected)?.value?.toFloat() ?: 0f
            Region(
                it.dptNb,
                it.dptLabel,
                calculateColorRegion(it, entrySelected, regionUiData.colorRegion),
                calculateColorRegion(it, entrySelected, regionUiData.colorSelectedRegion),
                value,
                toDateFormat.format(Date((it.series?.getOrNull(entrySelected)?.date ?: 0) * 1000)),
                formatValue(value),
            )
        } ?: listOf()
    }

    private fun calculateColorRegion(
        departmentKeyFigureMap: DepartmentKeyFigureMap,
        entrySelected: Int,
        @ColorInt baseColorRes: Int,
    ): Int {
        val valueDepartment = departmentKeyFigureMap.series?.getOrNull(entrySelected)?.opacity?.toFloat() ?: 0f
        val normalizedValue = valueDepartment / 100 * Constants.Map.RANGE_ALPHA_COLOR
        val clampedValue = min(normalizedValue.toInt(), Constants.Map.RANGE_ALPHA_COLOR)
        return ColorUtils.setAlphaComponent(baseColorRes, Constants.Map.MIN_ALPHA_COLOR + clampedValue)
    }

    fun formatValue(value: Float): String {
        val shortUnit = strings[chosenKeyFigure?.shortUnitStringKey] ?: ""
        return "$value$shortUnit"
    }
}

class MapFigureViewModelFactory(
    private val strings: LocalizedStrings,
    private val keyFigureMapRepository: KeyFigureMapRepository,
    private val keyFigureRepository: KeyFigureRepository,
    private val regionUiData: RegionUiData,
    private val toDateFormat: DateFormat,
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MapFigureViewModel(
            strings,
            keyFigureMapRepository,
            keyFigureRepository,
            regionUiData,
            toDateFormat,
        ) as T
    }
}
