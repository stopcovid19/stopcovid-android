/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository

class KeyFigureChartsFragmentViewModel(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel() {

    var keyFigure: KeyFigure? = null

    suspend fun setKeyFiguresWithLabel(label: String?) {
        keyFigure = label?.let { keyFigureRepository.keyFigureWithDepartmentByLabel(it).data }
    }
}

class KeyFigureChartsFragmentViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return KeyFigureChartsFragmentViewModel(
            keyFigureRepository,
        ) as T
    }
}
