/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class DetailFigureMapBottomSheetViewModel(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel() {
    private val _keyFigure: MutableStateFlow<KeyFigure?> = MutableStateFlow(null)
    val keyFigure: StateFlow<KeyFigure?> = _keyFigure

    fun fetchKeyFigure(label: String) {
        viewModelScope.launch {
            _keyFigure.value = keyFigureRepository.keyFigureByLabel(label).data
        }
    }
}

class DetailFigureMapBottomSheetViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return DetailFigureMapBottomSheetViewModel(
            keyFigureRepository,
        ) as T
    }
}
