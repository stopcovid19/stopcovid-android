/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.LayoutDirection
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityManager
import android.webkit.URLUtil
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.view.MenuHost
import androidx.core.view.MenuItemCompat
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.common.extension.getFileThumbnailFromFeaturedInfo
import com.lunabeestudio.common.extension.hasChosenPostalCode
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.isNightMode
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.HorizontalRecyclerViewScrollSaver
import com.lunabeestudio.stopcovid.core.fastitem.SpaceItem
import com.lunabeestudio.stopcovid.core.fastitem.buttonItem
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.horizontalRecyclerViewItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.core.model.CardTheme
import com.lunabeestudio.stopcovid.databinding.FragmentRecyclerViewFabBinding
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.colorStringKey
import com.lunabeestudio.stopcovid.extension.formatNumberIfNeeded
import com.lunabeestudio.stopcovid.extension.getKeyFigureForPostalCode
import com.lunabeestudio.stopcovid.extension.getRelativeDateTimeString
import com.lunabeestudio.stopcovid.extension.getString
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.isLowStorage
import com.lunabeestudio.stopcovid.extension.labelShortStringKey
import com.lunabeestudio.stopcovid.extension.lowStorageAlertShown
import com.lunabeestudio.stopcovid.extension.notificationVersionClosed
import com.lunabeestudio.stopcovid.extension.observeEventAndConsume
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showPostalCodeDialog
import com.lunabeestudio.stopcovid.fastitem.HomeScreenFigureCardItem
import com.lunabeestudio.stopcovid.fastitem.ItemWeight
import com.lunabeestudio.stopcovid.fastitem.bigTitleItem
import com.lunabeestudio.stopcovid.fastitem.compareFigureCardChartItem
import com.lunabeestudio.stopcovid.fastitem.explanationActionCardItem
import com.lunabeestudio.stopcovid.fastitem.homeFeaturedItem
import com.lunabeestudio.stopcovid.fastitem.homeScreeInfoCardItem
import com.lunabeestudio.stopcovid.fastitem.homeScreenFigureCardItem
import com.lunabeestudio.stopcovid.fastitem.homeScreenSeeAllCardItem
import com.lunabeestudio.stopcovid.fastitem.logoItem
import com.lunabeestudio.stopcovid.fastitem.simpleActionItem
import com.lunabeestudio.stopcovid.fastitem.smallQrCodeCardItem
import com.lunabeestudio.stopcovid.manager.AppMaintenanceManager
import com.lunabeestudio.stopcovid.manager.ChartManager
import com.lunabeestudio.stopcovid.manager.DeeplinkManager
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.DeeplinkOrigin
import com.lunabeestudio.stopcovid.model.KeyFigureFragmentState
import com.lunabeestudio.stopcovid.utils.ExtendedFloatingActionButtonScrollListener
import com.lunabeestudio.stopcovid.utils.lazyFast
import com.lunabeestudio.stopcovid.viewmodel.HomeViewModel
import com.lunabeestudio.stopcovid.viewmodel.HomeViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.text.NumberFormat
import java.util.concurrent.TimeUnit
import kotlin.time.Duration.Companion.seconds

class HomeFragment : TimeMainFragment() {

    override val layout: Int = R.layout.fragment_recycler_view_fab

    private var fragmentRecyclerViewFabBinding: FragmentRecyclerViewFabBinding? = null

    private val numberFormat: NumberFormat by lazyFast { NumberFormat.getNumberInstance(context.getApplicationLocale()) }

    private val accessibilityManager by lazy {
        context?.let { getSystemService(it, AccessibilityManager::class.java) }
    }

    private val keyFigureHorizontalRecyclerViewScrollSaver = HorizontalRecyclerViewScrollSaver()
    private val infoHorizontalRecyclerViewScrollSaver = HorizontalRecyclerViewScrollSaver()
    private val featuredInfoHorizontalRecyclerViewScrollSaver = HorizontalRecyclerViewScrollSaver()

    private val viewModel: HomeViewModel by viewModels {
        HomeViewModelFactory(
            configurationManager,
            walletRepository,
            sharedPrefs,
            injectionContainer.getSmartWalletMapUseCase,
            injectionContainer.getSmartWalletStateUseCase,
            injectionContainer.keyFigureRepository,
            injectionContainer.infoCenterRepository,
            injectionContainer.refreshInfoCenterUseCase,
            injectionContainer.getCompareKeyFiguresUseCase,
            injectionContainer.getFavoriteKeyFiguresUseCase,
            injectionContainer.keyFigureMapRepository,
            injectionContainer.featuredInfoRepository,
        )
    }

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val barcodeEncoder by lazyFast { BarcodeEncoder() }

    private var activityResultLauncher: ActivityResultLauncher<Intent>? = null

    private var shouldRefresh: Boolean = false
    private var lastAdapterRefresh: Long = 0L
    private val recyclerViewPool = RecyclerView.RecycledViewPool()
    private val menuProvider = object : MenuProvider {
        override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
            menuInflater.inflate(R.menu.home_access_menu, menu)
        }

        override fun onPrepareMenu(menu: Menu) {
            menu.findItem(R.id.item_qr_code_scanner)?.let { item ->
                MenuItemCompat.setContentDescription(
                    item,
                    strings["accessibility.hint.universalQrScanController.button"],
                )
            }
        }

        override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
            return when (menuItem.itemId) {
                R.id.item_qr_code_scanner -> {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToNavUniversalQrScan(),
                    )
                    true
                }
                else -> false
            }
        }
    }

    override fun getTitleKey(): String = "home.title.deactivated"

    private val sharedPreferenceChangeListener =
        SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            if (key == Constants.SharedPrefs.HAS_NEWS) {
                refreshItems()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setFragmentResultListener(UniversalQrScanFragment.SCANNED_CODE_RESULT_KEY) { _, bundle ->
            val scannedData = bundle.getString(UniversalQrScanFragment.SCANNED_CODE_BUNDLE_KEY)
            scannedData?.let { data ->
                if (URLUtil.isValidUrl(data)) {
                    val uri = Uri.parse(data).buildUpon()
                        .appendQueryParameter(
                            DeeplinkManager.DEEPLINK_CERTIFICATE_ORIGIN_PARAMETER,
                            DeeplinkOrigin.UNIVERSAL.name,
                        )
                        .build()
                    val finalUri = DeeplinkManager.transformFragmentToCodeParam(uri)
                    (activity as? MainActivity)?.processDeeplink(finalUri)
                } else {
                    lifecycleScope.launch {
                        findNavControllerOrNull()
                            ?.safeNavigate(
                                HomeFragmentDirections.actionHomeFragmentToWalletContainerFragment(
                                    code = data,
                                    deeplinkOrigin = DeeplinkOrigin.UNIVERSAL,
                                ),
                            )
                    }
                }
            }
        }
        activityResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    refreshScreen()
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        fragmentRecyclerViewFabBinding = view?.let { FragmentRecyclerViewFabBinding.bind(it) }

        setupExtendedFab()

        viewLifecycleOwner.lifecycleScope.launch {
            val isLowStorage = context?.isLowStorage() ?: false
            if (isLowStorage && !sharedPrefs.lowStorageAlertShown) {
                sharedPrefs.lowStorageAlertShown = true
                findNavControllerOrNull()
                    ?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToLowStorageBottomSheetFragment())
            } else if (!isLowStorage) {
                sharedPrefs.lowStorageAlertShown = false
            }
        }

        return view
    }

    private val setAccessibilityView: (Boolean) -> Unit = {
        (activity as? MenuHost)?.removeMenuProvider(menuProvider)
        if (it) {
            if (isResumed) {
                (activity as? MenuHost)?.addMenuProvider(menuProvider, viewLifecycleOwner, Lifecycle.State.RESUMED)
            }
            fragmentRecyclerViewFabBinding?.floatingActionButton?.isVisible = false
        } else {
            fragmentRecyclerViewFabBinding?.floatingActionButton?.isVisible = true
        }
    }

    private fun setupAccessibility() {
        setAccessibilityView(accessibilityManager?.isTouchExplorationEnabled == true)
        accessibilityManager?.addTouchExplorationStateChangeListener(setAccessibilityView)
    }

    private fun setupExtendedFab() {
        fragmentRecyclerViewFabBinding?.floatingActionButton?.let { fab ->
            fab.text = strings["home.qrScan.button.title"]
            fab.setOnClickListener {
                findNavControllerOrNull()?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToNavUniversalQrScan())
            }
            binding?.recyclerView?.addOnScrollListener(
                ExtendedFloatingActionButtonScrollListener(
                    fab,
                ),
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModelObserver()
        initHasNewsObserver()

        shouldRefresh = false

        findNavControllerOrNull()?.currentBackStackEntry?.savedStateHandle
            ?.getLiveData<Boolean>(PostalCodeBottomSheetFragment.SHOULD_BE_REFRESHED_KEY)?.observe(
                viewLifecycleOwner,
            ) { shouldBeRefreshed ->
                if (shouldBeRefreshed) {
                    refreshScreen()
                }
            }
    }

    override fun onResume() {
        super.onResume()
        if (shouldRefresh) {
            refreshScreen()
            shouldRefresh = false
        }
    }

    private fun initViewModelObserver() {
        viewModel.loadingInProgress.observe(viewLifecycleOwner) { loadingInProgress ->
            (activity as? MainActivity)?.showProgress(loadingInProgress)
        }
        viewModel.keyFigures.observe(viewLifecycleOwner) {
            refreshScreen()
        }
        viewModel.favKeyFigures.observe(viewLifecycleOwner) {
            refreshScreen()
        }
        viewModel.infoCenterData.observe(viewLifecycleOwner) {
            refreshScreen()
        }
        viewModel.favoriteDcc.observeEventAndConsume(viewLifecycleOwner) {
            refreshScreen()
        }
        viewModel.smartWalletMap.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }

        viewModel.hasKeyFigureMap.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }

        viewModel.featuredInfo.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }
    }

    override fun refreshScreen() {
        setupAccessibility()
        fragmentRecyclerViewFabBinding?.floatingActionButton?.text =
            strings["home.qrScan.button.title"]
        super.refreshScreen()
    }

    private fun initHasNewsObserver() {
        sharedPrefs.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener)
    }

    override fun onPause() {
        super.onPause()
        shouldRefresh = true
        accessibilityManager?.removeTouchExplorationStateChangeListener(setAccessibilityView)
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = mutableListOf<GenericItem>()

        val notification = configurationManager.configuration.notification
        if (notification != null && sharedPrefs.notificationVersionClosed < notification.version) {
            addNotificationItem(items, notification)
        }

        addTopImageItems(items)
        addScanQRCodeButton(items)

        if (AppMaintenanceManager.shouldDisplayUpdateAvailable) {
            addAppUpdateItems(items)
        }
        addSectionSeparator(items)

        // Wallet
        if (configurationManager.configuration.displaySanitaryCertificatesWallet) {
            addWalletItems(items)
            addSectionSeparator(items)
        }

        // Featuring items
        if (configurationManager.configuration.displayInTheSpotlight) {
            if (viewModel.featuredInfo.value.isNotEmpty()) {
                addFeaturedItem(items)
            }
        }

        // Vaccination items
        if (configurationManager.configuration.displayHomeVaccination) {
            addVaccinationSectionItems(items)
            if (configurationManager.configuration.displayVaccination) {
                addVaccinationItems(items)
            }
            addSectionSeparator(items)
        }

        if (configurationManager.configuration.displayHomeRecommendations) {
            // Contact tracing items
            val tracingItems = mutableListOf<GenericItem>()
            tracingItems += getHealthItems()
            tracingItems += getDisabledDeclareItems()

            if (tracingItems.isNotEmpty()) {
                items += bigTitleItem {
                    text = strings["home.contactSection.title"]
                    identifier = "home.contactSection.title".hashCode().toLong()
                    importantForAccessibility = ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO
                }
                items += tracingItems
                addSectionSeparator(items)
            }
        }

        if (viewModel.keyFigures.value != null && configurationManager.configuration.displayedHomeKeyFigures) {
            addFiguresItems(items)
            addSectionSeparator(items)
        }
        // News items
        addNewsItems(items)

        // More items
        addMoreItems(items)
        addSectionSeparator(items)

        refreshItems()

        return items
    }

    private fun addSectionSeparator(items: MutableList<GenericItem>) {
        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.count().toLong()
        }
    }

    private fun addAppUpdateItems(items: MutableList<GenericItem>) {
        items += cardWithActionItem {
            mainTitle = strings["home.appUpdate.cell.title"]
            mainBody = strings["home.appUpdate.cell.subtitle"]
            identifier = items.count().toLong()
            mainImage = R.drawable.app_update_card
            mainLayoutDirection = LayoutDirection.RTL
            onCardClick = {
                if (!ConfigConstant.Store.GOOGLE.openInExternalBrowser(requireContext(), false)) {
                    if (!ConfigConstant.Store.HUAWEI.openInExternalBrowser(
                            requireContext(),
                            false,
                        )
                    ) {
                        ConfigConstant.Store.TAC_WEBSITE.openInExternalBrowser(requireContext())
                    }
                }
            }
        }
    }

    private fun addNotificationItem(
        items: MutableList<GenericItem>,
        notification: Configuration.Notification,
    ) {
        val title = strings[notification.title] ?: return
        val subtitle = strings[notification.subtitle] ?: return
        val url = strings[notification.url] ?: return

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = R.drawable.ic_notif_envelope.toLong()
        }

        items += cardWithActionItem {
            mainTitle = title
            mainBody = subtitle
            mainImage = R.drawable.ic_notif_envelope
            mainLayoutDirection = LayoutDirection.LTR
            onCardClick = {
                url.openInExternalBrowser(requireContext())
            }
            onDismissClick = {
                sharedPrefs.notificationVersionClosed = notification.version
                refreshScreen()
            }
            identifier = R.drawable.ic_notif_envelope.toLong() + 1
        }
    }

    private fun addTopImageItems(items: MutableList<GenericItem>) {
        items += logoItem {
            imageRes = R.drawable.phone_hands_color
            identifier = R.id.item_logo.toLong()
        }
    }

    private fun addScanQRCodeButton(items: MutableList<GenericItem>) {
        items += buttonItem {
            text = strings["home.qrScan.button.title"]
            topMarginRes = null
            width = ViewGroup.LayoutParams.MATCH_PARENT
            onClickListener = View.OnClickListener {
                findNavControllerOrNull()?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToNavUniversalQrScan())
            }
            identifier = "home.qrScan.button.title".hashCode().toLong()
        }
    }

    private fun showTracingFeatureDeactivatedBottomSheet() {
        findNavControllerOrNull()
            ?.safeNavigate(
                HomeFragmentDirections
                    .actionHomeFragmentToTracingDeactivatedBottomSheetDialogFragment(),
            )
    }

    private fun addVaccinationSectionItems(items: MutableList<GenericItem>) {
        items += bigTitleItem {
            text = strings["home.vaccinationSection.title"]
            identifier = "home.vaccinationSection.title".hashCode().toLong()
            importantForAccessibility = ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO
        }
        if (configurationManager.configuration.displayUrgentDgs) {
            items += cardWithActionItem {
                onCardClick = {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToUrgentInfoFragment(),
                    )
                }
                mainImage = R.drawable.ic_dgsurgent
                mainTitle = strings["home.healthSection.dgsUrgent.title"]
                mainBody = strings["home.healthSection.dgsUrgent.subtitle"]
                identifier = "home.healthSection.dgsUrgent.title".hashCode().toLong()
            }
            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
        }
    }

    private fun getHealthItems(): List<GenericItem> {
        val items = mutableListOf<GenericItem>()

        items += cardWithActionItem(CardTheme.Primary) {
            mainImage = R.drawable.health_card
            mainTitle = strings["home.healthSection.contactTips.title"]
            mainBody = strings["home.healthSection.contactTips.subtitle"]
            onCardClick = {
                findNavControllerOrNull()?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToHealthFragment())
            }
            identifier = R.drawable.health_card.toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }

        return items
    }

    private fun addFiguresItems(items: MutableList<GenericItem>) {
        val keyFiguresResult = viewModel.keyFigures.value
        val localKeyFiguresFetchError = (keyFiguresResult as? TacResult.Failure)?.throwable
        if (localKeyFiguresFetchError is AppError && localKeyFiguresFetchError.code == AppError.Code.KEY_FIGURES_NOT_AVAILABLE) {
            addNoConnectionItem(items, localKeyFiguresFetchError)
        } else {
            items += bigTitleItem {
                text = strings["home.infoSection.keyFigures"]
                linkText = strings["home.figuresSection.all"]
                onClickLink = View.OnClickListener {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToKeyFiguresPagerFragment(),
                    )
                }
                identifier = "home.infoSection.keyFigures".hashCode().toLong()
            }
            if (!strings["home.infoSection.warning"].isNullOrBlank()) {
                items += cardWithActionItem {
                    mainBody = strings["home.infoSection.warning"]
                    mainMaxLines = 3
                    onCardClick = {
                        findNavControllerOrNull()
                            ?.safeNavigate(
                                HomeFragmentDirections.actionHomeFragmentToInfoSectionWarningBottomSheetFragment(),
                            )
                    }
                    identifier = "home.infoSection.warning".hashCode().toLong()
                }
                items += spaceItem {
                    spaceRes = R.dimen.spacing_medium
                    identifier = items.count().toLong()
                }
            }
            val favoriteFigures = viewModel.favKeyFigures.value
            if (favoriteFigures.isNullOrEmpty()) {
                items += cardWithActionItem {
                    mainTitle = strings["keyfigures.noFavorite.cell.title"]
                    mainBody = strings["keyfigures.noFavorite.cell.add.subtitle"]
                    onCardClick = {
                        findNavControllerOrNull()?.safeNavigate(
                            HomeFragmentDirections.actionHomeFragmentToKeyFiguresPagerFragment(
                                KeyFigureFragmentState.ALL,
                            ),
                        )
                    }
                    identifier = "keyfigures.noFavorite.cell.title".hashCode().toLong()
                }
                items += spaceItem {
                    spaceRes = R.dimen.spacing_medium
                }
            } else {
                addKeyFiguresRecycler(items, favoriteFigures)
            }

            addPostalCodeItem(items)

            addCompareChartIfNeeded(items)

            if (viewModel.hasKeyFigureMap.value && configurationManager.configuration.isKeyFigureMapFrDisplayed) {
                addMapItem(items)
            }
        }
    }

    private fun addMapItem(items: MutableList<GenericItem>) {
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += cardWithActionItem {
            mainTitle = strings["home.infoSection.keyFigures.map"]
            mainLayoutDirection = LayoutDirection.RTL
            mainImage = R.drawable.ic_france
            mainBody = strings["home.infoSection.keyfigures.map.subtitle"]
            contentDescription = strings["home.infoSection.keyfigures.map.subtitle"]
            onCardClick = {
                findNavControllerOrNull()
                    ?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToMapFiguresFragment())
            }
            identifier = "home.infoSection.keyFigures.map".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
    }

    private fun getItemFromKeyFigure(keyFigure: KeyFigure): HomeScreenFigureCardItem {
        val darkMode = requireContext().isNightMode()
        val block: (HomeScreenFigureCardItem.() -> Unit) = {
            strings[keyFigure.colorStringKey(darkMode)]?.let {
                colorBackground = Color.parseColor(it)
            }
            onClick = View.OnClickListener {
                findNavControllerOrNull()?.safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToKeyFigureDetailsFragment(
                        keyFigure.labelKey,
                    ),
                )
            }
            figureText = strings[keyFigure.labelShortStringKey]
            identifier = keyFigure.labelShortStringKey.hashCode().toLong()
            if (sharedPrefs.hasChosenPostalCode && configurationManager.configuration.displayDepartmentLevel) {
                valueText = keyFigure.getKeyFigureForPostalCode(sharedPrefs.chosenPostalCode, configurationManager)
                    ?.valueToDisplay
                    ?.formatNumberIfNeeded(numberFormat)
                    ?: keyFigure.valueGlobalToDisplay.formatNumberIfNeeded(numberFormat)
                regionText =
                    keyFigure.getKeyFigureForPostalCode(sharedPrefs.chosenPostalCode, configurationManager)?.dptLabel
                        ?: strings["common.country.france"]
            } else {
                valueText = keyFigure.valueGlobalToDisplay.formatNumberIfNeeded(numberFormat)
            }
        }

        return homeScreenFigureCardItem(block)
    }

    private fun addKeyFiguresRecycler(
        items: MutableList<GenericItem>,
        keyFigures: List<KeyFigure>,
    ) {
        items += horizontalRecyclerViewItem(R.id.item_horizontal_recyclerview_key_figure) {
            viewPool = recyclerViewPool
            identifier = "keyfigureRecyclerview".hashCode().toLong()
            horizontalRecyclerViewScrollSaver = keyFigureHorizontalRecyclerViewScrollSaver

            val arrayListItems = arrayListOf<GenericItem>()
            arrayListItems += spaceItem {
                spaceRes = R.dimen.spacing_large
                orientation = SpaceItem.Orientation.HORIZONTAL
            }
            keyFigures.forEach {
                arrayListItems += getItemFromKeyFigure(it)
                arrayListItems += spaceItem {
                    spaceRes = R.dimen.spacing_medium
                    orientation = SpaceItem.Orientation.HORIZONTAL
                }
            }
            arrayListItems += homeScreenSeeAllCardItem {
                seeAllText = strings["home.figuresSection.all"]
                onClick = View.OnClickListener {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToKeyFiguresPagerFragment(),
                    )
                }
                identifier = "home.figuresSection.all".hashCode().toLong()
            }
            arrayListItems += spaceItem {
                spaceRes = R.dimen.spacing_large
                orientation = SpaceItem.Orientation.HORIZONTAL
            }

            horizontalItems = arrayListItems
        }
    }

    private fun addNoConnectionItem(
        items: MutableList<GenericItem>,
        localKeyFiguresFetchError: AppError,
    ) {
        items += bigTitleItem {
            text = strings["home.infoSection.keyFigures"]
            identifier = "home.infoSection.keyFigures".hashCode().toLong()
            importantForAccessibility = ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO
        }
        items += explanationActionCardItem {
            explanation = localKeyFiguresFetchError.getString(strings)
            bottomText = strings["keyFiguresController.fetchError.button"]
            onClick = {
                viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                    (activity as? MainActivity)?.showProgress(true)
                    viewModel.refreshKeyFigures()
                    viewModel.refreshNews()
                    (activity as? MainActivity)?.showProgress(false)
                    refreshScreen()
                }
            }
            identifier = "keyFiguresController.fetchError.button".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }
    }

    private fun addPostalCodeItem(items: MutableList<GenericItem>) {
        if (configurationManager.configuration.displayDepartmentLevel) {
            if (sharedPrefs.chosenPostalCode == null) {
                items += cardWithActionItem(CardTheme.Primary) {
                    cardTitle = strings["home.infoSection.newPostalCode"]
                    cardTitleIcon = R.drawable.ic_map
                    mainBody = strings["home.infoSection.newPostalCode.subtitle"]
                    onCardClick = {
                        showPostalCodeDialog()
                    }
                    actions = listOf(
                        Action(label = strings["home.infoSection.newPostalCode.button"]) {
                            showPostalCodeDialog()
                        },
                    )
                    contentDescription = strings["home.infoSection.newPostalCode.subtitle"]
                    identifier = "home.infoSection.newPostalCode".hashCode().toLong()
                }
            } else {
                items += simpleActionItem {
                    label = stringsFormat("common.updatePostalCode", sharedPrefs.chosenPostalCode)
                    endLabel = strings["common.updatePostalCode.end"]
                    iconRes = R.drawable.ic_map
                    onClickListener = View.OnClickListener {
                        findNavControllerOrNull()
                            ?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToPostalCodeBottomSheetFragment())
                    }
                    identifier = "common.updatePostalCode".hashCode().toLong()
                }
            }
            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
        }
    }

    private fun addCompareChartIfNeeded(items: MutableList<GenericItem>) {
        val keyFigurePair = viewModel.getCompareKeyFigures()
        val ctx = this.context

        if (keyFigurePair != null && ctx != null) {
            items += compareFigureCardChartItem {
                shareContentDescription = strings["accessibility.hint.keyFigure.chart.share"]
                chartExplanationLabel = strings["home.figuresSection.keyFigures.chart.footer"]
                onClickListener = View.OnClickListener {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToCompareKeyFiguresFragment(),
                    )
                }
                onShareCard = { binding ->
                    ShareManager.shareChart(
                        this@HomeFragment,
                        binding,
                    )
                }
                localizedStrings = strings
                this.durationToShow = ChartManager.ChartRange.ALL.rangeDuration
                this.keyFigurePair = keyFigurePair
                isChartAnimated = false
                identifier = "home.figuresSection.keyFigures.chart.footer".hashCode().toLong()
            }
        }
    }

    private fun addFeaturedItem(items: MutableList<GenericItem>) {
        val featured = viewModel.featuredInfo.value
        items += bigTitleItem {
            text = strings["home.featuredInfoSection.title"]
            identifier = "home.featuredInfoSection.title".hashCode().toLong()
        }
        if (featured.size == 1) {
            items += getFeatureInfoItemFromData(featured[0], ItemWeight.MatchParent)
        } else {
            items += horizontalRecyclerViewItem(R.id.item_horizontal_recyclerview_featured) {
                horizontalRecyclerViewScrollSaver = featuredInfoHorizontalRecyclerViewScrollSaver

                viewPool = recyclerViewPool
                identifier = "featuredRecyclerView".hashCode().toLong()
                val arrayListItems = arrayListOf<GenericItem>()
                arrayListItems += spaceItem {
                    spaceRes = R.dimen.spacing_large
                    orientation = SpaceItem.Orientation.HORIZONTAL
                }
                featured.forEach { item ->
                    arrayListItems += getFeatureInfoItemFromData(
                        item,
                        ItemWeight.Weight(Constants.FeaturedInfo.RATIO_HOME_ITEM),
                    )
                    arrayListItems += spaceItem {
                        spaceRes = R.dimen.spacing_large
                        orientation = SpaceItem.Orientation.HORIZONTAL
                    }
                }
                horizontalItems = arrayListItems
            }
        }
    }

    private fun getFeatureInfoItemFromData(featuredInfo: FeaturedInfo, itemWeight: ItemWeight): GenericItem {
        return homeFeaturedItem {
            title = featuredInfo.title
            refreshButtonText = strings["common.retry"]
            weight = itemWeight
            thumbnailFile = context?.let { context ->
                File(context.filesDir, featuredInfo.getFileThumbnailFromFeaturedInfo())
            }
            identifier = featuredInfo.title.hashCode().toLong()
            onClick = View.OnClickListener {
                findNavControllerOrNull()?.safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToFullScreenFeaturedInfoFragment(featuredInfo.index),
                )
            }
            refreshButtonOnClick = View.OnClickListener {
                this.showLoading()
                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.refreshImageFeaturedInfo()
                    refreshScreen()
                }
            }
        }
    }

    private fun addNewsItems(items: MutableList<GenericItem>) {
        val data = viewModel.infoCenterData.value ?: return

        if (data.entries.isNotEmpty()) {
            items += bigTitleItem {
                text = strings["home.infoSection.news"]
                linkText = strings["home.infoSection.all"]
                onClickLink = View.OnClickListener {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToInfoCenterFragment(),
                    )
                }
                identifier = "home.infoSection.news".hashCode().toLong()
            }
            items += horizontalRecyclerViewItem(R.id.item_horizontal_recyclerview_info) {
                val arrayListItems = arrayListOf<GenericItem>()

                horizontalRecyclerViewScrollSaver = infoHorizontalRecyclerViewScrollSaver

                arrayListItems += spaceItem {
                    spaceRes = R.dimen.spacing_large
                    orientation = SpaceItem.Orientation.HORIZONTAL
                }
                data.entries.forEach { info ->
                    arrayListItems += homeScreeInfoCardItem {
                        titleText = info.title
                        captionText = info.timestamp.seconds.getRelativeDateTimeString(
                            requireContext(),
                            strings,
                        )
                        subtitleText = info.description
                        identifier = info.id
                        onClick = View.OnClickListener {
                            findNavControllerOrNull()?.safeNavigate(
                                HomeFragmentDirections.actionHomeFragmentToInfoCenterFragment(
                                    infoIdentifier = info.id,
                                ),
                            )
                        }
                    }
                    arrayListItems += spaceItem {
                        spaceRes = R.dimen.spacing_medium
                        orientation = SpaceItem.Orientation.HORIZONTAL
                    }
                }

                arrayListItems += homeScreenSeeAllCardItem {
                    seeAllText = strings["home.infoSection.all"]
                    onClick = View.OnClickListener {
                        findNavControllerOrNull()?.safeNavigate(
                            HomeFragmentDirections.actionHomeFragmentToInfoCenterFragment(),
                        )
                    }
                    identifier = "home.infoSection.all".hashCode().toLong()
                }

                arrayListItems += spaceItem {
                    spaceRes = R.dimen.spacing_large
                    orientation = SpaceItem.Orientation.HORIZONTAL
                }

                viewPool = recyclerViewPool
                horizontalItems = arrayListItems
                identifier = "infosRecyclerView".hashCode().toLong()
            }

            if (configurationManager.configuration.displayHomeUsefulLinks) {
                items += cardWithActionItem {
                    onCardClick = {
                        findNavControllerOrNull()?.safeNavigate(
                            HomeFragmentDirections.actionHomeFragmentToLinksFragment(),
                        )
                    }
                    mainImage = R.drawable.ic_useful
                    mainTitle = strings["home.infoSection.usefulLinks"]
                    mainBody = strings["home.infoSection.usefulLinks.subtitle"]
                    identifier = "home.infoSection.usefulLinks".hashCode().toLong()
                }
                items += spaceItem {
                    spaceRes = R.dimen.spacing_medium
                    identifier = items.count().toLong()
                }
            }

            addSectionSeparator(items)
        }
    }

    private fun addWalletItems(items: MutableList<GenericItem>) {
        items += bigTitleItem {
            text = strings["home.walletSection.title"]
            identifier = "home.walletSection.title".hashCode().toLong()
            importantForAccessibility = ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO
        }

        val favoriteDcc = viewModel.favoriteDcc.value?.peekContent()
        if (favoriteDcc != null) {
            items += smallQrCodeCardItem {
                val qrCodeSize = R.dimen.card_image_height.toDimensSize(requireContext()).toInt()
                title = strings["home.walletSection.favoriteCertificate.cell.title"]
                body = strings["home.walletSection.favoriteCertificate.cell.subtitle"]
                generateBarcode = {
                    barcodeEncoder.encodeBitmap(
                        favoriteDcc.value,
                        BarcodeFormat.QR_CODE,
                        qrCodeSize,
                        qrCodeSize,
                    )
                }
                onClick = {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToWalletFullscreenDccFragment(
                            id = favoriteDcc.id,
                        ),
                    )
                }
                identifier = favoriteDcc.id.hashCode().toLong()
            }
            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
        }

        val walletState = viewModel.getWalletState()
        items += cardWithActionItem(walletState.theme) {
            mainImage = walletState.icon
            mainLayoutDirection = LayoutDirection.RTL
            onCardClick = {
                findNavControllerOrNull()?.safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToWalletContainerFragment(),
                )
            }
            mainTitle = strings["home.attestationSection.sanitaryCertificates.cell.title"]
            mainBody = strings[walletState.bodyKey]
            identifier = R.drawable.wallet_card.toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }
    }

    private fun showPostalCodeDialog() {
        MaterialAlertDialogBuilder(requireContext()).showPostalCodeDialog(
            layoutInflater,
            strings,
            this,
            sharedPrefs,
        )
    }

    private fun getDisabledDeclareItems(): List<GenericItem> {
        val items = mutableListOf<GenericItem>()
        items += cardWithActionItem(cardTheme = CardTheme.Disabled) {
            mainTitle = strings["home.declareSection.noTracing.cellTitle"]
            mainBody = strings["home.declareSection.noTracing.cellSubtitle"]
            mainImage = R.drawable.declare_card
            contentDescription = strings["home.declareSection.title"]
            onCardClick = {
                showTracingFeatureDeactivatedBottomSheet()
            }
            identifier = "home.declareSection.noTracing.cellTitle".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }
        return items
    }

    private fun addVaccinationItems(items: MutableList<GenericItem>) {
        items += cardWithActionItem {
            mainTitle = strings["home.vaccinationSection.cellTitle"]
            mainLayoutDirection = LayoutDirection.RTL
            mainTitleColorRes = R.color.color_no_risk
            mainImage = R.drawable.ic_vaccine_center
            mainTitleIcon = R.drawable.ic_vaccin
            mainBody = strings["home.vaccinationSection.cellSubtitle"]
            contentDescription = strings["home.vaccinationSection.cellTitle"]
            onCardClick = {
                findNavControllerOrNull()?.safeNavigate(
                    HomeFragmentDirections.actionHomeFragmentToVaccinationFragment(),
                )
            }
            identifier = "home.vaccinationSection.cellTitle".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }
    }

    private fun addMoreItems(items: MutableList<GenericItem>) {
        items += bigTitleItem {
            text = strings["home.moreSection.title"]
            identifier = "home.moreSection.title".hashCode().toLong()
            importantForAccessibility = ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO
        }

        items += cardWithActionItem {
            actions = listOfNotNull(
                Action(R.drawable.ic_share, strings["home.moreSection.appSharing"]) {
                    ShareCompat.IntentBuilder(requireActivity())
                        .setType("text/plain")
                        .setText(strings["sharingController.appSharingMessage"])
                        .startChooser()
                },
                Action(R.drawable.ic_settings, strings["common.settings"]) {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToSettingsFragment(),
                    )
                },
                Action(R.drawable.ic_privacy, strings["home.moreSection.privacy"]) {
                    findNavControllerOrNull()?.safeNavigate(
                        HomeFragmentDirections.actionHomeFragmentToPrivacyFragment(),
                    )
                },
                Action(R.drawable.ic_about, strings["home.moreSection.aboutStopCovid"]) {
                    findNavControllerOrNull()?.safeNavigate(HomeFragmentDirections.actionHomeFragmentToAboutFragment())
                },
            )
            identifier = "home.moreSection.title.content".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.count().toLong()
        }
    }

    private fun refreshItems() {
        if (strings.isEmpty()) {
            return // Do nothing until strings are loaded
        }

        context?.let {
            if (binding?.recyclerView?.isComputingLayout == false) {
                lastAdapterRefresh = System.currentTimeMillis()
                binding?.recyclerView?.adapter?.notifyDataSetChanged()
            }
        }
    }

    override fun timeRefresh() {
        val notifyCalledMoreThanHalfAMinuteAgo =
            System.currentTimeMillis() - lastAdapterRefresh > TimeUnit.SECONDS.toMillis(30)
        if (notifyCalledMoreThanHalfAMinuteAgo) {
            viewModel.nowTimeMs.value = System.currentTimeMillis()
            viewLifecycleOwnerOrNull()?.lifecycleScope?.launch(Dispatchers.Default) {
                if (binding?.recyclerView?.isComputingLayout == false) {
                    withContext(Dispatchers.Main) {
                        lastAdapterRefresh = System.currentTimeMillis()
                        binding?.recyclerView?.adapter?.notifyDataSetChanged()
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(sharedPreferenceChangeListener)
        infoHorizontalRecyclerViewScrollSaver.updateStateFromRecycler()
        featuredInfoHorizontalRecyclerViewScrollSaver.updateStateFromRecycler()
        keyFigureHorizontalRecyclerViewScrollSaver.updateStateFromRecycler()

        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        infoHorizontalRecyclerViewScrollSaver.detachRecycler()
        featuredInfoHorizontalRecyclerViewScrollSaver.detachRecycler()
        keyFigureHorizontalRecyclerViewScrollSaver.detachRecycler()
    }
}
