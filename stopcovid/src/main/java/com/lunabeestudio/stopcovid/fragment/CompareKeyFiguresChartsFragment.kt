/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.ItemKeyFigureChartCardBinding
import com.lunabeestudio.stopcovid.extension.getSerializableCompat
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.fastitem.compareFigureCardChartItem
import com.lunabeestudio.stopcovid.manager.ChartManager
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.ChartFullScreenData
import com.lunabeestudio.stopcovid.utils.lazyFast
import com.lunabeestudio.stopcovid.viewmodel.CompareKeyFigureChartFragmentViewModel
import com.lunabeestudio.stopcovid.viewmodel.CompareKeyFigureChartFragmentViewModelFactory
import kotlin.time.Duration
import kotlinx.coroutines.launch

class CompareKeyFiguresChartsFragment : BaseFragment() {

    private val durationToShow: Duration by lazyFast {
        val range = arguments?.getSerializableCompat(RANGE_ARG_KEY) ?: ChartManager.ChartRange.ALL
        range.rangeDuration
    }

    private val viewModel: CompareKeyFigureChartFragmentViewModel by viewModels {
        CompareKeyFigureChartFragmentViewModelFactory(injectionContainer.keyFigureRepository)
    }

    private fun getChartOnClickListener(labelKey: String, labelKey2: String): View.OnClickListener =
        View.OnClickListener {
            parentFragment?.findNavControllerOrNull()?.safeNavigate(
                CompareKeyFiguresFragmentDirections.actionCompareKeyFiguresFragmentToChartFullScreenActivity(
                    ChartFullScreenData(
                        keyFigureKey = labelKey,
                        chartDataType = ChartDataType.GLOBAL,
                        durationToShow = durationToShow,
                        keyFigureKey2 = labelKey2,
                    ),
                ),
            )
        }

    override fun refreshScreen() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FrameLayout(inflater.context).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
            )
            id = R.id.keyfigures_charts_container
        }.also {
            lifecycleScope.launch {
                viewModel.setKeyFiguresWithLabel(
                    arguments?.getString(LABEL_KEY_ARG_KEY1),
                    arguments?.getString(LABEL_KEY_ARG_KEY2),
                )
                it.addView(getChartItem(it))
            }
        }
    }

    private fun getChartItem(container: ViewGroup?): View {
        val keyFigure1 = viewModel.keyFigure1
        val keyFigure2 = viewModel.keyFigure2

        return ItemKeyFigureChartCardBinding.inflate(layoutInflater, container, false).apply {
            keyFigureCombinedChart.isVisible = true
            compareFigureCardChartItem {
                shareContentDescription = this@CompareKeyFiguresChartsFragment.strings["accessibility.hint.keyFigure.chart.share"]
                chartExplanationLabel = this@CompareKeyFiguresChartsFragment.strings["keyfigures.comparison.chart.footer"]
                viewModel.keyFigure1?.let { figure1 ->
                    viewModel.keyFigure2?.let { figure2 ->
                        onClickListener = getChartOnClickListener(figure1.labelKey, figure2.labelKey)
                    }
                }
                onShareCard = { binding ->
                    ShareManager.shareChart(
                        this@CompareKeyFiguresChartsFragment,
                        binding,
                    )
                }

                this.keyFigurePair = if (keyFigure1 != null && keyFigure2 != null) {
                    Pair(keyFigure1, keyFigure2)
                } else {
                    null
                }

                this.durationToShow = this@CompareKeyFiguresChartsFragment.durationToShow
                this.localizedStrings = this@CompareKeyFiguresChartsFragment.strings
            }.bindView(this, emptyList())
        }.root.apply {
            updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin += resources.getDimensionPixelSize(R.dimen.spacing_large)
            }
        }
    }

    companion object {
        private const val LABEL_KEY_ARG_KEY1 = "LABEL_KEY_ARG_KEY1"
        private const val LABEL_KEY_ARG_KEY2 = "LABEL_KEY_ARG_KEY2"
        private const val RANGE_ARG_KEY = "RANGE_ARG_KEY"

        fun newInstance(labelKey1: String?, labelKey2: String?, range: ChartManager.ChartRange?): CompareKeyFiguresChartsFragment {
            return CompareKeyFiguresChartsFragment().apply {
                arguments = bundleOf(
                    LABEL_KEY_ARG_KEY1 to labelKey1,
                    LABEL_KEY_ARG_KEY2 to labelKey2,
                    RANGE_ARG_KEY to range,
                )
            }
        }
    }
}
