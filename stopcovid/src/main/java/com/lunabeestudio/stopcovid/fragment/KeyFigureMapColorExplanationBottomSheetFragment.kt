/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/02/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.view.View
import android.view.ViewGroup
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.secondaryButtonItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.fragment.FastAdapterBottomSheetDialogFragment
import com.mikepenz.fastadapter.GenericItem

class KeyFigureMapColorExplanationBottomSheetFragment : FastAdapterBottomSheetDialogFragment() {

    override fun refreshScreen() {
        val items = ArrayList<GenericItem>()
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.size.toLong()
        }
        items += captionItem {
            text = strings["keyfigures.map.screen.displayMethod.description"]
            identifier = "explanation".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.size.toLong()
        }
        items += secondaryButtonItem {
            text = strings["common.ok"]
            identifier = "common.ok".hashCode().toLong()
            width = ViewGroup.LayoutParams.MATCH_PARENT
            onClickListener = View.OnClickListener {
                this@KeyFigureMapColorExplanationBottomSheetFragment.dismiss()
            }
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
            identifier = items.size.toLong()
        }

        adapter.setNewList(items)
    }
}
