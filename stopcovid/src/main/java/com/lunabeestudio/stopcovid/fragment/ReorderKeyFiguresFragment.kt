/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/12 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import android.view.accessibility.AccessibilityManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.extension.descriptionStringKey
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.labelStringKey
import com.lunabeestudio.stopcovid.fastitem.reorderItem
import com.lunabeestudio.stopcovid.viewmodel.ReorderKeyFiguresViewModel
import com.lunabeestudio.stopcovid.viewmodel.ReorderKeyFiguresViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import com.mikepenz.fastadapter.drag.IDraggable
import com.mikepenz.fastadapter.drag.ItemTouchCallback
import com.mikepenz.fastadapter.drag.SimpleDragCallback
import com.mikepenz.fastadapter.utils.DragDropUtil

class ReorderKeyFiguresFragment : MainFragment(), ItemTouchCallback {

    override fun getTitleKey(): String = "keyfigures.reorder.button.title"

    private var selectedViewHolder: RecyclerView.ViewHolder? = null

    private val accessibilityManager by lazy {
        context?.let { ContextCompat.getSystemService(it, AccessibilityManager::class.java) }
    }

    private val viewModel: ReorderKeyFiguresViewModel by viewModels {
        ReorderKeyFiguresViewModelFactory(
            injectionContainer.getFavoriteKeyFiguresUseCase,
            injectionContainer.reorderFavoriteKeyFiguresUseCase,
        )
    }

    private val setAccessibilityView: (Boolean) -> Unit = {
        refreshScreen()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.favFigures.observe(viewLifecycleOwner) {
            refreshScreen()
        }
        setupTouchHelper()
        accessibilityManager?.addTouchExplorationStateChangeListener(setAccessibilityView)
    }

    private fun setupTouchHelper() {
        val dragCallback = SimpleDragCallback(this)
        val touchHelper = ItemTouchHelper(dragCallback) // Create ItemTouchHelper and pass with parameter the SimpleDragCallback
        touchHelper.attachToRecyclerView(binding?.recyclerView) // Attach ItemTouchHelper to RecyclerView
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = mutableListOf<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
        }

        items += captionItem {
            text = strings["keyfigures.reorder.description"]
            identifier = "keyfigures.reorder.description".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
        }

        viewModel.favFigures.value?.forEachIndexed { index, keyFigure ->
            items += reorderItem {
                title = strings[keyFigure.labelStringKey]
                subtitle = strings[keyFigure.descriptionStringKey]
                isFirst = index == 0
                isLast = (index + 1) == viewModel.favFigures.value?.size
                onUpClick = {
                    viewModel.reorderFavorite(index, index - 1)
                }
                upContentDescription = stringsFormat("accessibility.hint.keyFigures.order.up", title)
                upAfterClickContentDescription = stringsFormat("accessibility.feedback.keyFigures.order.up", title)
                onDownClick = {
                    viewModel.reorderFavorite(index, index + 1)
                }
                downContentDescription = stringsFormat("accessibility.hint.keyFigures.order.down", title)
                downAfterClickContentDescription = stringsFormat("accessibility.feedback.keyFigures.order.down", title)
                isAccessibilityOn = accessibilityManager?.isTouchExplorationEnabled ?: false
                identifier = keyFigure.labelKey.hashCode().toLong()
            }
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
        }

        return items
    }

    override fun itemTouchOnMove(oldPosition: Int, newPosition: Int): Boolean {
        // Determine the "drop area"
        val firstDropPosition = fastAdapter.adapterItems.indexOfFirst { it is IDraggable }
        val lastDropPosition = fastAdapter.adapterItems.indexOfLast { it is IDraggable }

        // Only move the item if the new position is inside the "drop area"
        return if (newPosition in firstDropPosition..lastDropPosition) {
            // Change the item's position in the adapter
            DragDropUtil.onMove(fastAdapter, oldPosition, newPosition)
            true
        } else {
            false
        }
    }

    override fun itemTouchDropped(oldPosition: Int, newPosition: Int) {
        val firstDropPosition = fastAdapter.adapterItems.indexOfFirst { it is IDraggable }
        viewModel.reorderFavorite(oldPosition - firstDropPosition, newPosition - firstDropPosition)
        selectedViewHolder?.itemView?.background = null
        selectedViewHolder = null
    }

    override fun itemTouchStartDrag(viewHolder: RecyclerView.ViewHolder) {
        selectedViewHolder = viewHolder
        selectedViewHolder?.itemView?.setBackgroundColor(
            ContextCompat.getColor(viewHolder.itemView.context, R.color.color_primary_light),
        )
    }

    override fun onDestroyView() {
        accessibilityManager?.removeTouchExplorationStateChangeListener(setAccessibilityView)
        super.onDestroyView()
    }
}
