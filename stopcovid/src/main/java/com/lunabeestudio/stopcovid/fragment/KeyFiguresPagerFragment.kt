/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/29/10 - for the STOP-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.view.MenuCompat
import androidx.core.view.MenuHost
import androidx.core.view.MenuItemCompat
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.refreshLift
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentKeyFiguresPagerBinding
import com.lunabeestudio.stopcovid.extension.getSerializableCompat
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.ratingsKeyFiguresOpening
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showPostalCodeDialog
import com.lunabeestudio.stopcovid.model.KeyFigureFragmentState
import com.lunabeestudio.stopcovid.viewmodel.KeyFiguresPagerViewModel
import com.lunabeestudio.stopcovid.viewmodel.KeyFiguresPagerViewModelFactory
import kotlinx.coroutines.launch

class KeyFiguresPagerFragment : BaseFragment() {

    private lateinit var viewPager: ViewPager2
    private var tabLayoutMediator: TabLayoutMediator? = null
    private var tabSelectedListener: TabLayout.OnTabSelectedListener? = null
    private lateinit var binding: FragmentKeyFiguresPagerBinding

    private val args: KeyFiguresPagerFragmentArgs by navArgs()

    private val viewModel: KeyFiguresPagerViewModel by viewModels {
        KeyFiguresPagerViewModelFactory(injectionContainer.getFavoriteKeyFiguresUseCase)
    }

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPrefs.ratingsKeyFiguresOpening++
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentKeyFiguresPagerBinding.inflate(inflater, container, false)
        viewPager = binding.viewPager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager()
        binding.viewPager.setCurrentItem(args.keyFigureFragmentState.pagerPosition, false)
        addMenuProvider()
        viewModel.favFigures.observe(viewLifecycleOwner) {
            (activity as? MenuHost)?.invalidateMenu()
        }
    }

    private fun addMenuProvider() {
        (activity as? MenuHost)?.addMenuProvider(
            object : MenuProvider {
                @SuppressLint("RestrictedApi")
                override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                    menuInflater.inflate(R.menu.key_figures_menu, menu)
                    (menu as? MenuBuilder)?.setOptionalIconsVisible(true)
                    MenuCompat.setGroupDividerEnabled(menu, true)
                }

                override fun onPrepareMenu(menu: Menu) {
                    val displayDepartmentLevel = injectionContainer.configurationManager.configuration.displayDepartmentLevel
                    menu.findItem(R.id.item_compare).title = strings["keyfigures.comparison.screen.title"]
                    MenuItemCompat.setContentDescription(
                        menu.findItem(R.id.item_compare),
                        strings["keyfigures.comparison.screen.title"],
                    )
                    menu.findItem(R.id.item_reorder).title = strings["keyfigures.reorder.button.title"]
                    MenuItemCompat.setContentDescription(
                        menu.findItem(R.id.item_reorder),
                        strings["keyfigures.reorder.button.title"],
                    )
                    menu.findItem(R.id.item_enter_postal_code).title = strings["home.infoSection.updatePostalCode.alert.newPostalCode"]
                    MenuItemCompat.setContentDescription(
                        menu.findItem(R.id.item_enter_postal_code),
                        strings["home.infoSection.updatePostalCode.alert.newPostalCode"],
                    )
                    menu.findItem(R.id.item_enter_postal_code).isVisible = displayDepartmentLevel
                    menu.findItem(R.id.item_delete_postal_code).title = strings["home.infoSection.updatePostalCode.alert.deletePostalCode"]
                    MenuItemCompat.setContentDescription(
                        menu.findItem(R.id.item_delete_postal_code),
                        strings["home.infoSection.updatePostalCode.alert.deletePostalCode"],
                    )

                    menu.findItem(R.id.item_reorder).isEnabled = (viewModel.favFigures.value?.size ?: 0) > 1
                    menu.findItem(R.id.item_delete_postal_code).isVisible = sharedPrefs.chosenPostalCode != null && displayDepartmentLevel
                }

                override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                    return when (menuItem.itemId) {
                        R.id.item_compare -> {
                            findNavControllerOrNull()?.safeNavigate(
                                KeyFiguresPagerFragmentDirections.actionKeyFiguresPagerFragmentToCompareKeyFiguresFragment(),
                            )
                            true
                        }
                        R.id.item_reorder -> {
                            findNavControllerOrNull()?.safeNavigate(
                                KeyFiguresPagerFragmentDirections.actionKeyFiguresPagerFragmentToReorderKeyFiguresFragment(),
                            )
                            true
                        }
                        R.id.item_enter_postal_code -> {
                            MaterialAlertDialogBuilder(requireContext()).showPostalCodeDialog(
                                layoutInflater = layoutInflater,
                                strings = strings,
                                baseFragment = this@KeyFiguresPagerFragment,
                                sharedPrefs = sharedPrefs,
                            )
                            true
                        }
                        R.id.item_delete_postal_code -> {
                            viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                                (activity as? MainActivity)?.showProgress(true)
                                injectionContainer.deletePostalCodeUseCase()
                                (activity as? MainActivity)?.showProgress(false)
                            }
                            true
                        }
                        else -> false
                    }
                }
            },
            viewLifecycleOwner,
            Lifecycle.State.RESUMED,
        )
    }

    override fun refreshScreen() {
        (activity as? MainActivity)?.binding?.tabLayout?.isVisible = true
    }

    private fun setupViewPager() {
        viewPager.adapter = KeyFiguresPagerAdapter()
        (activity as? MainActivity)?.binding?.tabLayout?.let { tabLayout ->
            tabLayoutMediator = TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = when (position) {
                    KeyFigureFragmentState.FAVORITE.pagerPosition -> strings["keyFiguresController.favorites.segment.title"]
                    KeyFigureFragmentState.ALL.pagerPosition -> strings["keyFiguresController.all.segment.title"]
                    else -> "Unknown tab"
                }
            }.also {
                it.attach()
            }
            tabSelectedListener = object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    view?.postDelayed(
                        {
                            val appBarLayout = (activity as? MainActivity)?.binding?.appBarLayout ?: return@postDelayed

                            // Force invalidate cached target view
                            appBarLayout.liftOnScrollTargetViewId = R.id.recycler_view

                            // Refresh current lift state
                            val selectedState = when (tab?.position) {
                                KeyFigureFragmentState.FAVORITE.pagerPosition -> KeyFigureFragmentState.FAVORITE
                                KeyFigureFragmentState.ALL.pagerPosition -> KeyFigureFragmentState.ALL
                                else -> null
                            }

                            val fragment = childFragmentManager.fragments.firstOrNull { fragment ->
                                val state = fragment.arguments
                                    ?.getSerializableCompat<KeyFigureFragmentState>(KeyFiguresFragment.STATE_ARG_KEY)
                                state == selectedState
                            }
                            fragment?.view
                                ?.findViewById<RecyclerView>(R.id.recycler_view)?.let { recyclerView ->
                                    appBarLayout.refreshLift(recyclerView)
                                }
                        },
                        100,
                    )
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    /* no-op */
                }

                override fun onTabReselected(tab: TabLayout.Tab?) {
                    /* no-op */
                }
            }

            tabSelectedListener?.let { tabLayout.addOnTabSelectedListener(it) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        tabLayoutMediator?.detach()
        (activity as? MainActivity)?.binding?.tabLayout?.apply {
            isVisible = false
            tabSelectedListener?.let { removeOnTabSelectedListener(it) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::viewPager.isInitialized) {
            viewPager.adapter = null
        }
    }

    private inner class KeyFiguresPagerAdapter : FragmentStateAdapter(childFragmentManager, lifecycle) {
        override fun getItemCount(): Int = KeyFigureFragmentState.values().size

        override fun createFragment(position: Int): Fragment = KeyFigureFragmentState.getByPosition(position)?.let {
            KeyFiguresFragment.newInstance(it)
        } ?: throw AppError(AppError.Code.UNKNOWN, "No fragment for position $position")
    }
}
