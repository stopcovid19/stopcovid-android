/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/29/10 - for the STOP-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.extension.doOnFragmentAttached
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.viewmodel.WalletPagerViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletPagerViewModelFactory

class WalletPagerFragment : BaseFragment() {

    private lateinit var viewPager: ViewPager2
    private var tabLayoutMediator: TabLayoutMediator? = null
    private var tabSelectedListener: TabLayout.OnTabSelectedListener? = null

    private val viewModel by viewModels<WalletPagerViewModel> {
        WalletPagerViewModelFactory(
            injectionContainer.walletRepository,
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewPager = ViewPager2(inflater.context)
        viewPager.id = R.id.wallet_pager
        return viewPager
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager()
        viewModel.certificatesCount.observe(viewLifecycleOwner) { certificatesCount ->
            if (certificatesCount == 0) {
                (activity as? MainActivity)?.showProgress(false)
                findNavControllerOrNull()?.safeNavigate(
                    WalletPagerFragmentDirections.actionWalletPagerFragmentToWalletInfoFragment(),
                )
            } else if (certificatesCount != null) {
                (activity as? MainActivity)?.binding?.tabLayout?.getTabAt(walletCertificateListFragmentPosition)?.text =
                    stringsFormat("walletController.mode.myCertificates", certificatesCount)
            }
        }
    }

    override fun refreshScreen() {
        appCompatActivity?.supportActionBar?.title = strings["walletController.title"]

        (activity as? MainActivity)?.binding?.tabLayout?.let { tabLayout ->
            viewModel.certificatesCount.value?.let { certificatesCount ->
                tabLayout.getTabAt(walletCertificateListFragmentPosition)?.text = stringsFormat(
                    "walletController.mode.myCertificates",
                    certificatesCount,
                )
            }
            tabLayout.getTabAt(walletMultipassFragmentPosition)?.text = strings["multiPass.tab.title"]
            tabLayout.getTabAt(walletInfoFragmentPosition)?.text = strings["walletController.mode.info"]

            tabLayout.isVisible = true
        }
    }

    private fun setupViewPager() {
        val savedStateHandle = findNavControllerOrNull()?.currentBackStackEntry?.savedStateHandle

        val scrollToCertificateId = savedStateHandle?.get<String>(SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY)
        viewPager.adapter = WalletPagerAdapter(scrollToCertificateId)
        if (scrollToCertificateId != null) {
            viewPager.currentItem = walletCertificateListFragmentPosition
            savedStateHandle.remove<String>(SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY)
        }
        (activity as? MainActivity)?.binding?.tabLayout?.let { tabLayout ->
            tabLayoutMediator = TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = when (position) {
                    walletInfoFragmentPosition -> strings["walletController.mode.info"]
                    walletMultipassFragmentPosition -> strings["multiPass.tab.title"]
                    else -> null
                }
            }.also {
                it.attach()
            }

            tabSelectedListener = object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    val block: (FragmentManager, Fragment) -> Unit = { _, fragment ->
                        (fragment as? PagerTabFragment)?.onTabSelected()
                    }

                    when (tab?.position) {
                        walletCertificateListFragmentPosition -> childFragmentManager.doOnFragmentAttached<WalletCertificateListFragment>(
                            block,
                        )
                        walletMultipassFragmentPosition -> {
                            childFragmentManager.doOnFragmentAttached<WalletMultipassFragment>(block)
                        }
                        walletInfoFragmentPosition -> childFragmentManager.doOnFragmentAttached<WalletInfoFragment>(
                            block,
                        )
                        else -> throw AppError(AppError.Code.UNKNOWN, "No fragment for position ${tab?.position}")
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    /* no-op */
                }

                override fun onTabReselected(tab: TabLayout.Tab?) {
                    /* no-op */
                }
            }

            tabSelectedListener?.let {
                tabLayout.addOnTabSelectedListener(it)
            }
        }

        // Call onTabSelected on first fragment attached to emulate the tab selection
        childFragmentManager.doOnFragmentAttached<Fragment> { _, fragment ->
            (fragment as? PagerTabFragment)?.onTabSelected()
        }

        // Observe scrollToCertificateId to select certificates list on new multipass generated
        savedStateHandle?.getLiveData<String>(SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY)
            ?.observe(viewLifecycleOwner) { liveScrollToCertificateId ->
                if (liveScrollToCertificateId != null) {
                    savedStateHandle[SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY] = null
                    viewPager.currentItem = walletCertificateListFragmentPosition
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        tabLayoutMediator?.detach()
        (activity as? MainActivity)?.binding?.tabLayout?.let { tabLayout ->
            tabSelectedListener?.let { tabLayout.removeOnTabSelectedListener(it) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::viewPager.isInitialized) {
            viewPager.adapter = null
        }
    }

    private inner class WalletPagerAdapter(
        private val scrollToCertificateId: String?,
    ) : FragmentStateAdapter(childFragmentManager, lifecycle) {

        override fun getItemCount(): Int = viewpagerItemCount

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                walletCertificateListFragmentPosition -> WalletCertificateListFragment.getInstance(
                    scrollToCertificateId,
                )
                walletMultipassFragmentPosition -> WalletMultipassFragment()
                walletInfoFragmentPosition -> WalletInfoFragment()
                else -> throw AppError(AppError.Code.UNKNOWN, "No fragment for position $position")
            }
        }
    }

    private val viewpagerItemCount: Int
        get() = if (injectionContainer.configurationManager.configuration.multipassConfig?.isEnabled == true) {
            3
        } else {
            2
        }

    private val walletCertificateListFragmentPosition: Int
        get() = 0

    private val walletMultipassFragmentPosition: Int
        get() = if (injectionContainer.configurationManager.configuration.multipassConfig?.isEnabled == true) {
            1
        } else {
            -1
        }

    private val walletInfoFragmentPosition: Int
        get() = if (injectionContainer.configurationManager.configuration.multipassConfig?.isEnabled == true) {
            2
        } else {
            1
        }

    companion object {
        const val SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY: String = "SAVED_STATE_HANDLE_SCROLL_TO_CERTIFICATE_KEY"
    }
}
