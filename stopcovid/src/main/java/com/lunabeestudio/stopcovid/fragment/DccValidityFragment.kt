/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.Context
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.material.appbar.AppBarLayout
import com.lunabeestudio.domain.model.smartwallet.SmartWalletValidity
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.fetchSystemColor
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.fastitem.buttonItem
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.databinding.FragmentRecyclerViewKonfettiBinding
import com.lunabeestudio.stopcovid.extension.emitDefaultKonfetti
import com.lunabeestudio.stopcovid.extension.future
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.isSignatureExpired
import com.lunabeestudio.stopcovid.fastitem.logoItem
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.viewmodel.DccValidityViewModel
import com.lunabeestudio.stopcovid.viewmodel.DccValidityViewModelFactory
import com.lunabeestudio.stopcovid.worker.DccValidNotificationWorker
import com.mikepenz.fastadapter.GenericItem
import nl.dionsegijn.konfetti.ParticleSystem
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.TimeUnit

class DccValidityFragment : MainFragment() {

    private var fragmentRecyclerViewKonfettiBinding: FragmentRecyclerViewKonfettiBinding? = null

    override val layout: Int = R.layout.fragment_recycler_view_konfetti

    override fun getAppBarLayout(): AppBarLayout? {
        return fragmentRecyclerViewKonfettiBinding?.appBarLayout
    }

    private val args by navArgs<DccValidityFragmentArgs>()

    private val viewModel: DccValidityViewModel by viewModels {
        DccValidityViewModelFactory(
            walletRepository,
            injectionContainer.computeDccValidityUseCase,
        )
    }

    private val longDateFormat = SimpleDateFormat.getDateInstance(DateFormat.LONG, getApplicationLocale())
    private val noYearDateFormat = SimpleDateFormat("d MMMM", getApplicationLocale())

    private var reminderSet: Boolean = false
    private var konfettiEmitted = false
    private var konfettis: ParticleSystem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        konfettiEmitted = savedInstanceState?.getBoolean(SAVE_INSTANCE_KONFETTI_EMITTED) ?: konfettiEmitted
        reminderSet = savedInstanceState?.getBoolean(SAVE_INSTANCE_REMINDER_SET) ?: reminderSet
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        fragmentRecyclerViewKonfettiBinding = view?.let { FragmentRecyclerViewKonfettiBinding.bind(it) }
        setupToolbar()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showLoading()

        viewModel.showWalletEvent.observe(viewLifecycleOwner) {
            findNavControllerOrNull()?.popBackStack()
        }
    }

    private fun setupToolbar() {
        // remove shadow
        fragmentRecyclerViewKonfettiBinding?.appBarLayout?.outlineProvider = null
        // replace previous activity action bar
        appCompatActivity?.setSupportActionBar(fragmentRecyclerViewKonfettiBinding?.toolbar)
        // show back arrow
        appCompatActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // no title
        appCompatActivity?.supportActionBar?.title = null

        fragmentRecyclerViewKonfettiBinding?.toolbar?.let { toolbar ->
            findNavControllerOrNull()?.let { navController ->
                NavigationUI.setupWithNavController(
                    toolbar,
                    navController,
                )
            }
        }
    }

    private fun emitKonfetti() {
        requireView().performHapticFeedback(
            HapticFeedbackConstants.LONG_PRESS,
            HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING,
        )
        konfettis?.stop()
        konfettis = fragmentRecyclerViewKonfettiBinding?.konfettiView?.emitDefaultKonfetti(binding)
    }

    override fun getTitleKey(): String = ""

    override suspend fun getItems(): List<GenericItem> {
        val validity = viewModel.getValidity(args.certificateId)

        if (validity == null || validity.end?.let { it < Date() } == true) {
            findNavControllerOrNull()?.popBackStack()
            return emptyList()
        }

        val nowDate = Date()
        val validDate = validity.start ?: nowDate
        val isDccValidNow = validDate <= nowDate

        val items = mutableListOf<GenericItem>()

        items += logoItem {
            imageRes = R.drawable.ic_thumbsup
            imageTint = R.attr.colorPrimary.fetchSystemColor(requireContext())
            onClick = {
                emitKonfetti()
            }
            identifier = R.drawable.ic_thumbsup.toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }

        val stringStateKey = if (isDccValidNow) {
            VALID_STRING_KEY
        } else {
            PENDING_STRING_KEY
        }

        if (!konfettiEmitted && viewModel.shouldShowKonfetti(args.certificateId)) {
            konfettiEmitted = true
            emitKonfetti()
        }

        items += cardWithActionItem {
            val formattedDate = longDateFormat.format(validDate)
            mainTitle = stringsFormat("vaccineCompletionController.$stringStateKey.explanation.title", formattedDate)
            mainBody = stringsFormat("vaccineCompletionController.$stringStateKey.explanation.body", formattedDate)
            identifier = "vaccineCompletionController.$stringStateKey.explanation.title".hashCode().toLong()
        }

        if (!isDccValidNow && !reminderSet) {
            items.addNotifyItems(validDate, stringStateKey)
        }

        if (isDccValidNow || reminderSet) {
            items.addFavoriteItems()
        } else {
            items.addNotifyAndFavoriteItems(validDate)
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_large
        }

        return items
    }

    private fun MutableList<GenericItem>.addNotifyItems(validDate: Date, stringStateKey: String) {
        this += spaceItem {
            spaceRes = R.dimen.spacing_large
        }
        this += buttonItem {
            text = stringsFormat(
                "vaccineCompletionController.$stringStateKey.button.notifyMe.title",
                noYearDateFormat.format(validDate),
            )
            width = ViewGroup.LayoutParams.MATCH_PARENT
            bottomMarginRes = null
            onClickListener = View.OnClickListener {
                context?.let {
                    if (!reminderSet) {
                        notifyMe(it, validDate)
                    }
                }
            }
            identifier = "vaccineCompletionController.$stringStateKey.button.notifyMe.title".hashCode().toLong()
        }
        this += captionItem {
            text = stringsFormat(
                "vaccineCompletionController.footer.notify",
                noYearDateFormat.format(validDate),
            )
            textAppearance = R.style.TextAppearance_StopCovid_Caption_Small_Grey
            identifier = "vaccineCompletionController.footer.notify".hashCode().toLong()
        }
    }

    private fun MutableList<GenericItem>.addNotifyAndFavoriteItems(validDate: Date) {
        this += spaceItem {
            spaceRes = R.dimen.spacing_large
        }
        this += buttonItem {
            text = strings["vaccineCompletionController.button.notifyAndFavorite.title"]
            width = ViewGroup.LayoutParams.MATCH_PARENT
            bottomMarginRes = null
            onClickListener = View.OnClickListener {
                context?.let {
                    if (!reminderSet) {
                        notifyMe(it, validDate)
                    }
                }
                viewModel.addCertificateInFavorite(args.certificateId)
            }
            identifier = "vaccineCompletionController.button.notifyAndFavorite.title".hashCode().toLong()
        }
        this += captionItem {
            text = stringsFormat(
                "vaccineCompletionController.footer.notifyAndFavorite",
                noYearDateFormat.format(validDate),
            )
            textAppearance = R.style.TextAppearance_StopCovid_Caption_Small_Grey
            identifier = "vaccineCompletionController.footer.notifyAndFavorite".hashCode().toLong()
        }
    }

    private fun MutableList<GenericItem>.addFavoriteItems() {
        this += spaceItem {
            spaceRes = R.dimen.spacing_large
        }
        this += buttonItem {
            text = strings["vaccineCompletionController.button.favorite.title"]
            width = ViewGroup.LayoutParams.MATCH_PARENT
            bottomMarginRes = null
            onClickListener = View.OnClickListener {
                viewModel.addCertificateInFavorite(args.certificateId)
            }
            identifier = "vaccineCompletionController.button.favorite.title".hashCode().toLong()
        }
        this += captionItem {
            text = strings["vaccineCompletionController.footer.favorite"]
            textAppearance = R.style.TextAppearance_StopCovid_Caption_Small_Grey
            identifier = "vaccineCompletionController.footer.favorite".hashCode().toLong()
        }
    }

    private fun notifyMe(context: Context, validDate: Date) {
        val reminderWorker = OneTimeWorkRequestBuilder<DccValidNotificationWorker>()
            .setInitialDelay(validDate.time - System.currentTimeMillis(), TimeUnit.MILLISECONDS)
            .build()
        WorkManager.getInstance(context)
            .enqueueUniqueWork(Constants.WorkerNames.DCC_VALID_REMINDER, ExistingWorkPolicy.REPLACE, reminderWorker)

        strings["common.notifyMe.feedback"]?.let { message ->
            (activity as? MainActivity)?.showSnackBar(message)
        }

        reminderSet = true
        refreshScreen()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean(SAVE_INSTANCE_KONFETTI_EMITTED, konfettiEmitted)
        outState.putBoolean(SAVE_INSTANCE_REMINDER_SET, reminderSet)
    }

    companion object {
        private const val PENDING_STRING_KEY = "pending"
        private const val VALID_STRING_KEY = "completed"

        private const val SAVE_INSTANCE_REMINDER_SET = "save.instance.reminderSet"
        private const val SAVE_INSTANCE_KONFETTI_EMITTED = "save.instance.konfettiEmitted"

        fun shouldShowDccValidityFragment(
            validity: SmartWalletValidity?,
            europeanCertificate: EuropeanCertificate?,
        ): Boolean {
            if (validity == null || europeanCertificate == null) {
                return false
            }

            return validity.end?.future() ?: true &&
                !europeanCertificate.isSignatureExpired &&
                europeanCertificate.isBlacklisted != true
        }
    }
}
