/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.content.DialogInterface
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment

fun BaseFragment.showUnknownErrorAlert(listener: DialogInterface.OnDismissListener?) {
    context?.let { context ->
        MaterialAlertDialogBuilder(context)
            .setTitle(strings["common.error.unknown"])
            .setPositiveButton(strings["common.ok"], null)
            .setOnDismissListener(listener)
            .show()
    }
}
