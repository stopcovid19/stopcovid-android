/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/3 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.SharedPreferences
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.BlacklistRepository
import com.lunabeestudio.domain.syncmanager.BlacklistSyncManager
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.extension.blacklistRefreshedAt
import com.lunabeestudio.stopcovid.extension.isExpired
import com.lunabeestudio.stopcovid.extension.raw
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import io.mockk.Ordering
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import java.util.Date
import kotlin.test.assertIs

@ExperimentalCoroutinesApi
class UpdateBlacklistUseCaseTest {

    private val fakeWalletRepository = mutableListOf<WalletCertificate>()

    private val configurationManager = mockk<ConfigurationManager>(relaxUnitFun = true)
    private val walletRepository = mockk<WalletRepository>(relaxUnitFun = true)
    private val blacklistRepository = mockk<BlacklistRepository>(relaxUnitFun = true)
    private val blacklistSyncManager = mockk<BlacklistSyncManager>(relaxUnitFun = true)
    private val configuration = mockk<Configuration>(relaxUnitFun = true)
    private val sharedPreferences = mockk<SharedPreferences>(relaxUnitFun = true)
    private val computeDccValidityUseCase = mockk<ComputeDccValidityUseCase>(relaxUnitFun = true)

    private lateinit var useCase: UpdateBlacklistUseCase

    @Before
    fun init() {
        mockkStatic(SharedPreferences::blacklistRefreshedAt)
        mockkStatic(WalletCertificate::raw)

        every { walletRepository.walletCertificateFlow } answers {
            MutableStateFlow(TacResult.Success(fakeWalletRepository))
        }
        coEvery { walletRepository.getCertificateById(any()) } answers { fakeWalletRepository.first { it.id == firstArg() } }
        every { sharedPreferences.blacklistRefreshedAt } returns Date(0)
        every { sharedPreferences.blacklistRefreshedAt = any() } returns Unit
        every { configurationManager.configuration } returns configuration

        useCase = UpdateBlacklistUseCase(
            walletRepository = walletRepository,
            blacklistRepository = blacklistRepository,
            blacklistSyncManager = blacklistSyncManager,
            configurationManager = configurationManager,
            sharedPreferences = sharedPreferences,
            computeDccValidityUseCase = computeDccValidityUseCase,
        )
    }

    @Test
    fun dry_run() {
        coEvery { blacklistRepository.getMatchPrefix(any()) } returns null
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        coVerify(exactly = 1) { walletRepository.updateCertificate(emptyList()) }
    }

    @Test
    fun index_no_match() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "no-match"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(any()) } returns null
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(exactly = 1) { rawCertificate.isBlacklisted = false }
        coVerify(exactly = 1) { walletRepository.updateCertificate(listOf(rawCertificate)) }
    }

    @Test
    fun index_match_prefix_no_match() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val sha256 = "match-123456789abcdefghijklmnopqrst"
        val wrongPrefix = "${sha256.substring(0, Constants.Certificate.BLACKLIST_HASH_LENGTH - 1)}0"
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns sha256
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(sha256) } returns "match"
        coEvery { blacklistRepository.getPrefixHashes("match") } returns TacResult.Success(listOf(wrongPrefix))
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(exactly = 1) { rawCertificate.isBlacklisted = false }
        coVerify(exactly = 1) { walletRepository.updateCertificate(listOf(rawCertificate)) }
    }

    @Test
    fun index_match_prefix_match() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val sha256 = "match-123456789abcdefghijklmnopqrst"
        val prefix = sha256.substring(0, Constants.Certificate.BLACKLIST_HASH_LENGTH)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns sha256
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(sha256) } returns "match"
        coEvery { blacklistRepository.getPrefixHashes("match") } returns TacResult.Success(listOf(prefix))
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(exactly = 1) { rawCertificate.isBlacklisted = true }
        coVerify(exactly = 1) { walletRepository.updateCertificate(listOf(rawCertificate)) }
    }

    @Test
    fun index_match_prefix_match_then_prefix_no_match() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "match-12345"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix("match-12345") } returns "match"
        coEvery { blacklistRepository.getPrefixHashes("match") }.returnsMany(
            TacResult.Success(listOf("match-12345")),
            TacResult.Success(listOf("match-abcde")),
        )
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(ordering = Ordering.SEQUENCE) {
            rawCertificate.isBlacklisted = true
            rawCertificate.isBlacklisted = false
        }
        coVerify(exactly = 2) { walletRepository.updateCertificate(listOf(rawCertificate)) }
    }

    @Test
    fun index_match_prefix_match_then_index_no_match() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "match-12345"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix("match-12345") }.returnsMany(
            "match",
            null,
        )
        coEvery { blacklistRepository.getPrefixHashes("match") }.returnsMany(
            TacResult.Success(listOf("match-12345")),
        )
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(ordering = Ordering.SEQUENCE) {
            rawCertificate.isBlacklisted = true
            rawCertificate.isBlacklisted = false
        }
        coVerify(exactly = 2) { walletRepository.updateCertificate(listOf(rawCertificate)) }
        coVerify(exactly = 1) { blacklistRepository.getPrefixHashes("match") }
    }

    @Test
    fun index_sync_failed() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "no-match"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(any()) } returns null
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Failure())

        runTest {
            assertIs<TacResult.Failure<Unit>>(useCase())
        }

        verify(exactly = 0) { rawCertificate.isBlacklisted = any() }
        coVerify(exactly = 0) { walletRepository.updateCertificate(any<List<RawWalletCertificate>>()) }
        coVerify(exactly = 0) { walletRepository.updateCertificate(any<RawWalletCertificate>()) }
    }

    @Test
    fun index_sync_no_change_is_blacklisted() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "no-match"
            every { certificate.isBlacklisted } returns true
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(any()) } returns null
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(null))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(exactly = 0) { rawCertificate.isBlacklisted = any() }
        coVerify(exactly = 1) { walletRepository.updateCertificate(emptyList()) }
    }

    @Test
    fun index_sync_no_change_unknown_blacklisted() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "no-match"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(any()) } returns null
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(null))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(exactly = 1) { rawCertificate.isBlacklisted = false }
        coVerify(exactly = 1) { walletRepository.updateCertificate(listOf(rawCertificate)) }
    }

    @Test
    fun fetch_prefix_failed() {
        val rawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val certificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.sha256 } returns "match-12345"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns rawCertificate
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix("match-12345") } returns "match"
        coEvery { blacklistRepository.getPrefixHashes("match") } returns TacResult.Failure()
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        verify(exactly = 0) { rawCertificate.isBlacklisted = any() }
        coVerify(exactly = 1) { walletRepository.updateCertificate(emptyList()) }
    }

    @Test
    fun update_by_id() {
        val currentRawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val currentCertificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.id } returns "current"
            every { certificate.sha256 } returns "no-match"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns currentRawCertificate
        }
        fakeWalletRepository.add(currentCertificate)

        val newRawCertificate = mockk<RawWalletCertificate>(relaxUnitFun = true)
        val newCertificate = mockk<WalletCertificate>().also { certificate ->
            every { certificate.id } returns "new"
            every { certificate.sha256 } returns "match-12345"
            every { certificate.isBlacklisted } returns null
            every { certificate.raw } returns newRawCertificate
        }
        fakeWalletRepository.add(newCertificate)

        coEvery { blacklistRepository.getMatchPrefix("match-12345") } returns "match"
        coEvery { blacklistRepository.getMatchPrefix("no-match") } returns null
        coEvery { blacklistRepository.getPrefixHashes("match") } returns TacResult.Success(listOf("match-12345"))
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase(certificateId = "new"))
        }

        verify(exactly = 0) { currentRawCertificate.isBlacklisted = any() }
        verify(exactly = 1) { newRawCertificate.isBlacklisted = true }

        coVerify(exactly = 1) { walletRepository.updateCertificate(listOf(newRawCertificate)) }
    }

    @Test
    fun ignore_expired_dcc() {
        val certificate = mockk<EuropeanCertificate>().also { certificate ->
            every { certificate.isExpired(any(), any()) } returns true
        }
        fakeWalletRepository.add(certificate)
        coEvery { blacklistRepository.getMatchPrefix(any()) } returns null
        every { blacklistSyncManager.syncIndexes() } returns flowOf(TacResult.Success(Unit))
        every { computeDccValidityUseCase(any(), any()) } returns null

        runTest {
            assertIs<TacResult.Success<Unit>>(useCase())
        }

        coVerify(exactly = 1) { walletRepository.updateCertificate(emptyList()) }
    }
}
