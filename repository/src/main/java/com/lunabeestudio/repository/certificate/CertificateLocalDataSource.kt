/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.certificate

import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import kotlinx.coroutines.flow.Flow

interface CertificateLocalDataSource {
    val rawWalletCertificatesFlow: Flow<TacResult<List<RawWalletCertificate>>>
    suspend fun insertAllRawWalletCertificates(certificates: List<RawWalletCertificate>)
    suspend fun updateAllRawWalletCertificates(certificates: List<RawWalletCertificate>)
    suspend fun deleteRawWalletCertificate(certificateId: String)
    suspend fun deleteAllRawWalletCertificates()

    suspend fun getCertificateCount(): Int
    val certificateCountFlow: Flow<Int>
    fun getCertificateByIdFlow(id: String): Flow<RawWalletCertificate?>
    suspend fun getCertificateById(id: String): RawWalletCertificate?

    suspend fun forceRefreshCertificatesFlow()
    suspend fun deleteLostCertificates()

    fun resetKeyGeneratedFlag()
}
