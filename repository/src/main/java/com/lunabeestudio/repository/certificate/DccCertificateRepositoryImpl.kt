/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.certificate

import com.lunabeestudio.domain.model.DccCertificates
import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.repository.DccCertificateRepository
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import com.lunabeestudio.repository.file.JsonRemoteRepositoryImpl
import java.lang.reflect.Type
import javax.inject.Inject

class DccCertificateRepositoryImpl @Inject constructor(
    private val dccCertificateLocalDataSource: DccCertificateLocalDataSource,
    jsonLocalDataSource: JsonLocalDataSource,
    jsonRemoteDataSource: JsonRemoteDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : JsonRemoteRepositoryImpl<DccCertificates>(
    jsonLocalDataSource,
    jsonRemoteDataSource,
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
),
    DccCertificateRepository {

    override val type: Type = dccCertificateLocalDataSource.type

    override fun getLocalFileName(): String = dccCertificateLocalDataSource.getLocalFileName()

    override fun getRemoteFileUrl(): String = dccCertificateLocalDataSource.getRemoteFileUrl()

    override fun getAssetFilePath(): String = dccCertificateLocalDataSource.getAssetFilePath()

    override var certificates: DccCertificates = emptyMap()
        private set

    override suspend fun initialize() {
        certificates = loadLocal() ?: emptyMap()
    }

    override suspend fun onAppForeground() {
        if (fetchLast() == DownloadStatus.FromServer) {
            initialize()
        }
    }
}
