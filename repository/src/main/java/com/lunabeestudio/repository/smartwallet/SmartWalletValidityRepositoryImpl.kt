/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.smartwallet

import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.model.smartwallet.SmartWalletValidityPivot
import com.lunabeestudio.domain.repository.SmartWalletValidityRepository
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import com.lunabeestudio.repository.file.JsonRemoteRepositoryImpl
import com.lunabeestudio.repository.smartwallet.model.ApiSmartWalletValidityPivot
import java.lang.reflect.Type
import javax.inject.Inject

class SmartWalletValidityRepositoryImpl @Inject constructor(
    private val smartWalletValidityLocalDataSource: SmartWalletValidityLocalDataSource,
    jsonLocalDataSource: JsonLocalDataSource,
    jsonRemoteDataSource: JsonRemoteDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : JsonRemoteRepositoryImpl<List<ApiSmartWalletValidityPivot>>(
    jsonLocalDataSource,
    jsonRemoteDataSource,
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
),
    SmartWalletValidityRepository {

    override val type: Type = smartWalletValidityLocalDataSource.type
    override fun getLocalFileName(): String = smartWalletValidityLocalDataSource.getLocalFileName()
    override fun getRemoteFileUrl(): String = smartWalletValidityLocalDataSource.getRemoteFileUrl()
    override fun getAssetFilePath(): String = smartWalletValidityLocalDataSource.getAssetFilePath()

    override var smartWalletValidityPivot: List<SmartWalletValidityPivot<out Any>> = emptyList()
        private set

    override suspend fun initialize() {
        smartWalletValidityPivot = loadLocal()?.flatMap(ApiSmartWalletValidityPivot::toSmartWalletValidityPivots)
            .orEmpty()
            .sortedBy { it.startDate }
    }

    override suspend fun onAppForeground() {
        if (fetchLast() == DownloadStatus.FromServer) {
            initialize()
        }
    }
}
