/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/14 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.featuredinfo

import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.FeaturedInfoRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class FeaturedInfoRepositoryImpl @Inject constructor(
    private val featuredInfoRemoteDatasource: FeaturedInfoRemoteDataSource,
    private val featuredInfoLocalDatasource: FeaturedInfoLocalDataSource,
) : FeaturedInfoRepository {

    override fun flowAllFeaturedInfo(): Flow<List<FeaturedInfo>> {
        return featuredInfoLocalDatasource.flowAllFeaturedInfo()
    }

    override suspend fun getFeaturedInfoByIndex(index: Int): FeaturedInfo {
        return featuredInfoLocalDatasource.getFeaturedInfoByIndex(index)
    }

    override suspend fun initialize() {
        fetchNewData()
    }

    private suspend fun fetchNewData() {
        val lastLanguageSync = featuredInfoLocalDatasource.prevSyncLanguageFeaturedInfo
        val localSuffix = featuredInfoLocalDatasource.getApplicationLanguage()
        val forceNewData = lastLanguageSync != localSuffix
        val result = featuredInfoRemoteDatasource.fetchFeaturedInfo(localSuffix, forceNewData)
        if (result is TacResult.Success && result.successData != null) {
            result.successData?.let { featuredInfo ->
                featuredInfoLocalDatasource.prevSyncLanguageFeaturedInfo = localSuffix
                removeOldFiles(featuredInfoLocalDatasource.flowAllFeaturedInfo().first())
                featuredInfoLocalDatasource.deleteOldAndInsertNew(featuredInfo)
                featuredInfo.forEach {
                    saveResourceInLocal(it.thumbnail, featuredInfoLocalDatasource.getFileThumbnailFromFeaturedInfo(it))
                    saveResourceInLocal(it.hd, featuredInfoLocalDatasource.getFileNameHDFromFeaturedInfo(it))
                }
            }
        }
    }

    override suspend fun saveResourceInLocal(url: String, fileName: String) {
        featuredInfoRemoteDatasource.saveResourceInLocal(url, fileName)
    }

    private fun removeOldFiles(oldFeaturedInfo: List<FeaturedInfo>) {
        featuredInfoLocalDatasource.removeOldFiles(oldFeaturedInfo)
    }
}
