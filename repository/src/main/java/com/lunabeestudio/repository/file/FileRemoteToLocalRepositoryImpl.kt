/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.file

import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.repository.FileRemoteToLocalRepository
import com.lunabeestudio.error.RemoteError
import java.io.File
import java.io.InputStream

abstract class FileRemoteToLocalRepositoryImpl(
    private val fileLocalDataSource: FileLocalDataSource,
    private val fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : FileRemoteToLocalRepository {

    protected abstract fun getLocalFileName(): String
    protected abstract fun getRemoteFileUrl(): String
    protected abstract fun getAssetFilePath(): String?
    protected abstract val mimeType: String
    override val ignoreCache: Boolean = false

    protected suspend fun getLocalFileOrAssetStream(): InputStream? {
        return fileLocalDataSource.getLocalFileOrAssetStream(getLocalFileName(), getAssetFilePath())
    }

    protected open suspend fun fetchLast(): DownloadStatus {
        val tmpFileName = "${getLocalFileName()}.bck"
        val tmpFile = File(fileLocalDataSource.filesDir, tmpFileName)

        return try {
            val remoteFileUrl = getRemoteFileUrl()
            val saveSucceeded = fileRemoteToLocalDataSource.saveTo(
                remoteFileUrl,
                tmpFile.absolutePath,
                mimeType,
                ignoreCache,
            )
            if (saveSucceeded) {
                if (fileNotCorrupted(tmpFile)) {
                    tmpFile.copyTo(
                        File(fileLocalDataSource.filesDir, getLocalFileName()),
                        overwrite = true,
                        bufferSize = 4 * 1024,
                    )
                } else {
                    throw RemoteError(RemoteError.Code.BACKEND, "$tmpFile is corrupted")
                }
                DownloadStatus.FromServer
            } else {
                DownloadStatus.FromCache
            }
        } catch (e: Exception) {
            DownloadStatus.Failure
        } finally {
            tmpFile.delete()
        }
    }

    override fun clearLocal() {
        fileLocalDataSource.clearLocal(getLocalFileName())
    }
}
