/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.eutags

import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.repository.EUTagsRepository
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import com.lunabeestudio.repository.file.JsonRemoteRepositoryImpl
import java.lang.reflect.Type
import javax.inject.Inject

class EUTagsRepositoryImpl @Inject constructor(
    private val euTagsLocalDataSource: EUTagsLocalDataSource,
    jsonLocalDataSource: JsonLocalDataSource,
    jsonRemoteDataSource: JsonRemoteDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : JsonRemoteRepositoryImpl<List<String>>(
    jsonLocalDataSource,
    jsonRemoteDataSource,
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
),
    EUTagsRepository {

    override val type: Type = euTagsLocalDataSource.type
    override fun getLocalFileName(): String = euTagsLocalDataSource.getLocalFileName()
    override fun getRemoteFileUrl(): String = euTagsLocalDataSource.getRemoteFileUrl()
    override fun getAssetFilePath(): String = euTagsLocalDataSource.getAssetFilePath()

    private var tags: List<String>? = null

    override fun isEUTag(certificateManufacturer: String): Boolean {
        return tags?.contains(certificateManufacturer) == true
    }

    override suspend fun initialize() {
        loadLocal()?.let { localTags ->
            if (tags != localTags) {
                tags = localTags
            }
        }
    }

    override suspend fun onAppForeground() {
        if (fetchLast() == DownloadStatus.FromServer) {
            loadLocal()?.let { localTags ->
                if (this.tags != localTags) {
                    this.tags = localTags
                }
            }
        }
    }
}
