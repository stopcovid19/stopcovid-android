/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.keyfigures

import com.lunabeestudio.common.extension.toMapKeyFigures
import com.lunabeestudio.domain.di.KeyFigureBaseUrl
import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.remote.extension.fromCacheOrEtags
import com.lunabeestudio.repository.keyfigure.KeyFigureMapRemoteDataSource
import okhttp3.OkHttpClient
import javax.inject.Inject

class KeyFigureMapRemoteDataSourceImpl @Inject constructor(
    @KeyFigureBaseUrl baseUrl: String,
    okHttpClient: OkHttpClient,
) : KeyFigureMapRemoteDataSource {
    private val api: KeyFiguresMapApi = RetrofitClient.getProtoGzipService(
        baseUrl,
        KeyFiguresMapApi::class.java,
        okHttpClient,
    )

    override suspend fun fetchKeyFigureMap(): TacResult<List<KeyFigureMap>?> {
        return try {
            val response = api.getKeyFigureMap()
            when {
                response.fromCacheOrEtags -> TacResult.Success(null)
                response.isSuccessful -> TacResult.Success(response.body()?.toMapKeyFigures())
                else -> TacResult.Failure()
            }
        } catch (e: Exception) {
            TacResult.Failure()
        }
    }
}
