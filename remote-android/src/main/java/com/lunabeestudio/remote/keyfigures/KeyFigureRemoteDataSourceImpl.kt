/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.keyfigures

import com.lunabeestudio.domain.di.KeyFigureBaseUrl
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.remote.extension.fromCacheOrEtags
import com.lunabeestudio.common.extension.toKeyFigures
import com.lunabeestudio.repository.keyfigure.KeyFigureRemoteDataSource
import okhttp3.OkHttpClient
import javax.inject.Inject

class KeyFigureRemoteDataSourceImpl @Inject constructor(
    @KeyFigureBaseUrl baseUrl: String,
    okHttpClient: OkHttpClient,
) : KeyFigureRemoteDataSource {
    private val api: KeyFiguresApi = RetrofitClient.getProtoGzipService(
        baseUrl,
        KeyFiguresApi::class.java,
        okHttpClient,
    )

    override suspend fun fetchKeyFigures(codePath: String, localSuffix: String): TacResult<List<KeyFigure>?> {
        return try {
            val response = api.getKeyFigure(localSuffix, codePath)
            when {
                response.fromCacheOrEtags -> TacResult.Success(null)
                response.isSuccessful -> TacResult.Success(response.body()?.toKeyFigures())
                else -> TacResult.Failure()
            }
        } catch (e: Exception) {
            TacResult.Failure()
        }
    }
}
