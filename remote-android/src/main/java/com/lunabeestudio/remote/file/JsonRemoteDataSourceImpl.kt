/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.file

import android.util.MalformedJsonException
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import javax.inject.Inject

class JsonRemoteDataSourceImpl @Inject constructor() : JsonRemoteDataSource {

    override fun throwMalformedJsonException() {
        throw MalformedJsonException("Failed to parse JSON")
    }
}
