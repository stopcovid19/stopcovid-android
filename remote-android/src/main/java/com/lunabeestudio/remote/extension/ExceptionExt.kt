/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.extension

import android.util.MalformedJsonException
import com.google.gson.Gson
import com.lunabeestudio.error.RemoteError
import com.lunabeestudio.remote.model.ServerException
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

internal fun Exception.toRemoteException(): RemoteError = when (this) {
    is MalformedJsonException -> RemoteError(RemoteError.Code.BACKEND)
    is SSLException -> RemoteError(RemoteError.Code.BACKEND)
    is SocketTimeoutException,
    is IOException,
    -> RemoteError(RemoteError.Code.NO_INTERNET)
    is HttpException -> {
        val httpCode = code()
        try {
            when (httpCode) {
                401 -> RemoteError(RemoteError.Code.UNAUTHORIZED)
                403 -> RemoteError(RemoteError.Code.FORBIDDEN)
                else -> {
                    val serverError =
                        Gson().fromJson(this.response()?.errorBody()?.string() ?: "", ServerException::class.java)
                    serverError?.message?.let { message ->
                        RemoteError(RemoteError.Code.BACKEND, message)
                    } ?: RemoteError(RemoteError.Code.BACKEND)
                }
            }
        } catch (e: Exception) {
            RemoteError(RemoteError.Code.BACKEND)
        }
    }
    else -> RemoteError(RemoteError.Code.UNKNOWN)
}
