/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.extension

import java.net.HttpURLConnection

val retrofit2.Response<*>.fromCacheOrEtags: Boolean
    get() = this.raw().fromCacheOrEtags

val okhttp3.Response.fromCacheOrEtags: Boolean
    get() {
        val networkResponse = this.networkResponse
        return this.isSuccessful && (networkResponse == null || networkResponse.code == HttpURLConnection.HTTP_NOT_MODIFIED)
    }
