/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.info

import com.lunabeestudio.remote.model.ApiInfoCenterEntry
import com.lunabeestudio.remote.model.ApiInfoCenterLastUpdatedAt
import com.lunabeestudio.remote.model.ApiInfoCenterTag
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface InfoCenterApi {
    @Headers("Cache-Control: max-age=300")
    @GET("info-center-lastupdate.json")
    suspend fun getInfoCenterLastUpdate(): Response<ApiInfoCenterLastUpdatedAt>

    @GET("info-center.json")
    suspend fun getInfoCenter(): Response<List<ApiInfoCenterEntry>>

    @GET("info-tags.json")
    suspend fun getInfoTags(): Response<List<ApiInfoCenterTag>>

    @GET("info-labels-{localSuffix}.json")
    suspend fun getLabels(
        @Path("localSuffix") localSuffix: String,
    ): Response<Map<String, String>>
}
