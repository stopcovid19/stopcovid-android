/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.synchronization.infocenter

import com.lunabeestudio.domain.di.InfoCenterBaseUrl
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.syncmanager.InfoCenterSyncManager
import com.lunabeestudio.local.info.InfoCenterDao
import com.lunabeestudio.local.info.model.InfoCenterEntryRoom
import com.lunabeestudio.local.info.model.InfoCenterEntryTagCrossRefRoom
import com.lunabeestudio.local.info.model.InfoCenterLabelRoom
import com.lunabeestudio.local.info.model.InfoCenterTagRoom
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.remote.extension.fromCacheOrEtags
import com.lunabeestudio.remote.info.InfoCenterApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Response
import java.util.Date
import javax.inject.Inject

class InfoCenterSyncManagerImpl @Inject constructor(
    @InfoCenterBaseUrl baseUrl: String,
    okHttpClient: OkHttpClient,
    private val infoCenterDao: InfoCenterDao,
) : InfoCenterSyncManager {
    private val api: InfoCenterApi = RetrofitClient.getJsonService(baseUrl, InfoCenterApi::class.java, okHttpClient)

    private suspend fun <R> fetchInfoCenter(call: suspend () -> Response<R>): TacResult<R?> {
        return try {
            val response = call()
            when {
                response.fromCacheOrEtags -> TacResult.Success(null)
                response.isSuccessful -> TacResult.Success(response.body())
                else -> TacResult.Failure()
            }
        } catch (e: Exception) {
            TacResult.Failure()
        }
    }

    override suspend fun sync(localSuffix: String): Date? {
        val doSync: suspend () -> Boolean = suspend {
            withContext(Dispatchers.IO) {
                val infoTagsResult = fetchInfoCenter { api.getInfoTags() }
                val infoCenterEntriesResult = fetchInfoCenter { api.getInfoCenter() }
                val labelsResult = fetchInfoCenter { api.getLabels(localSuffix) }

                val infoCenterEntryRoom = mutableListOf<InfoCenterEntryRoom>()
                val infoCenterEntryTagCrossRefRoom = mutableListOf<InfoCenterEntryTagCrossRefRoom>()
                val infoCenterTagRoom = mutableListOf<InfoCenterTagRoom>()
                val infoCenterLabelRoom = mutableListOf<InfoCenterLabelRoom>()

                infoTagsResult.data?.let { apiList ->
                    apiList.mapTo(infoCenterTagRoom) { api ->
                        InfoCenterTagRoom(
                            tagLabelKey = api.labelKey,
                            colorCode = api.colorCode,
                        )
                    }
                }

                infoCenterEntriesResult.data?.let { apiList ->
                    apiList.mapTo(infoCenterEntryRoom) { api ->
                        InfoCenterEntryRoom(
                            infoCenterEntryId = api.titleKey.substringBefore('.'),
                            titleKey = api.titleKey,
                            descriptionKey = api.descriptionKey,
                            buttonLabelKey = api.buttonLabelKey,
                            urlKey = api.urlKey,
                            timestamp = api.timestamp,
                        )
                    }

                    apiList.flatMapTo(infoCenterEntryTagCrossRefRoom) { api ->
                        api.tagIds?.map { tagId ->
                            InfoCenterEntryTagCrossRefRoom(
                                infoCenterEntryId = api.titleKey.substringBefore('.'),
                                tagLabelKey = tagId,
                            )
                        } ?: emptyList()
                    }
                }

                labelsResult.data?.let { apiList ->
                    apiList.mapTo(infoCenterLabelRoom) { (key, value) ->
                        InfoCenterLabelRoom(
                            infoCenterLabelId = key,
                            label = value,
                        )
                    }
                }

                infoCenterDao.replaceAll(
                    infoCenterEntryRoom = infoCenterEntryRoom,
                    infoCenterLabelRoom = infoCenterLabelRoom,
                    infoCenterTagRoom = infoCenterTagRoom,
                    infoCenterEntryTagCrossRefRoom = infoCenterEntryTagCrossRefRoom,
                )

                // Tags are not mandatory
                infoCenterEntriesResult is TacResult.Success && labelsResult is TacResult.Success
            }
        }

        return withContext(Dispatchers.IO) {
            if (doSync()) {
                fetchInfoCenter { api.getInfoCenterLastUpdate() }.data?.lastUpdatedAt?.let(::Date)
            } else {
                null
            }
        }
    }
}
