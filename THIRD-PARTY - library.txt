================================================================================
Apache 2.0
================================================================================

Component: Kotlinx Coroutines
License Text URL: https://github.com/Kotlin/kotlinx.coroutines/blob/master/LICENSE.txt
Source Code: https://github.com/Kotlin/kotlinx.coroutines

Component: Timber
License Text URL: https://github.com/JakeWharton/timber/blob/master/LICENSE.txt
Source Code: https://github.com/JakeWharton/timber

Component: Progress button
License Text URL : http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/razir/ProgressButton

Component: QRCode image processing library
License Text URL: https://github.com/zxing/zxing/blob/master/LICENSE
Source Code: https://github.com/zxing/zxing

Component: Barcode scanning library for Android, using ZXing for decoding
License Text URL: http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/journeyapps/zxing-android-embedded

Component: PhotoView
License Text URL: http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/Baseflow/PhotoView

Component: EU Digital COVID Certificate App Core - Android
License Text URL: http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/eu-digital-green-certificates/dgca-app-core-android

Component: PdfBox-Android
License Text URL: http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/TomRoush/PdfBox-Android

Component: Treessence
License Text URL: http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/bastienpaulfr/Treessence

Component: RichPath
License Text URL: http://www.apache.org/licenses/LICENSE-2.0
Source Code: https://github.com/tarek360/RichPath

================================================================================
BSD 3-Clause
================================================================================

Component: Android Scanner Compat Library
License Text URL: https://github.com/NordicSemiconductor/Android-Scanner-Compat-Library/blob/master/LICENSE
Source Code: https://github.com/NordicSemiconductor/Android-Scanner-Compat-Library

================================================================================
ISC License
================================================================================

Component: Konfetti
License Text URL: https://opensource.org/licenses/ISC
Source Code: https://github.com/DanielMartinus/Konfetti