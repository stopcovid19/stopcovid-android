/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/25 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.model

data class InfoCenterData(
    val entries: List<InfoCenterEntry>,
) {
    data class InfoCenterEntry(
        val id: Long,
        val title: String,
        val description: String,
        val buttonLabel: String?,
        val url: String?,
        val timestamp: Long,
        val tags: List<InfoCenterTag>?,
    ) {
        data class InfoCenterTag(
            val label: String,
            val colorCode: String,
        )
    }
}
