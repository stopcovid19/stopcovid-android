/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.model

import com.lunabeestudio.domain.model.smartwallet.SmartWalletVacc
import kotlin.time.Duration

data class Configuration(
    var version: Int,
    var displayVaccination: Boolean,
    var displayDepartmentLevel: Boolean,
    var displaySanitaryCertificatesWallet: Boolean,
    var walletPublicKeys: List<WalletPublicKey>,
    var testCertificateValidityThresholds: List<Int>,
    var maxCertBeforeWarning: Int,
    var ratingsKeyFiguresOpeningThreshold: Int,
    var displayUrgentDgs: Boolean,
    var notification: Notification?,
    val generationServerPublicKey: String,
    val activityPassSkipNegTestHours: Int,
    val smartWalletVacc: SmartWalletVacc?,
    var smartWalletNotif: Boolean,
    var isSmartWalletOn: Boolean,
    var colorsCompareKeyFigures: ColorsCompareKeyFigures?,
    var keyFiguresCombination: List<KeyFigureCombination>?,
    var dccKidsEmoji: DccKidsEmoji?,
    var multipassConfig: MultipassConfig?,
    val smartWalletEngine: SmartWalletEngine?,
    val blacklistRefreshFrequency: Duration?,
    var notifElgDelay: Duration,
    var updateFilesAfterDays: Duration,
    var isKeyFigureMapFrDisplayed: Boolean,
    var displayedHomeKeyFigures: Boolean,
    var displayHomeRecommendations: Boolean,
    var displayHomeUsefulLinks: Boolean,
    var displayHomeVaccination: Boolean,
    var displayInTheSpotlight: Boolean,
) {
    class Notification(val title: String, val subtitle: String, val url: String, val version: Int)
    class ColorsCompareKeyFigures(
        val colorKeyFigure1: ColorCompareKeyFigures,
        val colorKeyFigure2: ColorCompareKeyFigures,
    )

    class ColorCompareKeyFigures(val darkColor: String?, val lightColor: String?)
    class KeyFigureCombination(val title: String?, val keyFigureLabel1: String?, val keyFigureLabel2: String?)
    class DccKidsEmoji(val age: Int, val emojis: List<String>)
    data class MultipassConfig(val isEnabled: Boolean, val testMaxDuration: Duration, val maxDcc: Int, val minDcc: Int)
    class SmartWalletEngine(val displayExp: Duration, val displayElg: Duration)
}
