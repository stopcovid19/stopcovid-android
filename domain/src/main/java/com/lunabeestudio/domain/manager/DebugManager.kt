/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.manager

import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import java.io.File

interface DebugManager {
    fun logSaveCertificates(rawWalletCertificate: RawWalletCertificate, info: String? = null)
    fun logDeleteCertificates(rawWalletCertificate: RawWalletCertificate, info: String? = null)
    fun logObserveCertificate(rawWalletCertificateResult: TacResult<List<RawWalletCertificate>>, info: String? = null)
    fun logOpenWalletContainer(rawWalletCertificateResult: TacResult<List<RawWalletCertificate>>, info: String? = null)
    fun logReinitializeWallet()
    suspend fun zip(files: List<File>, toZipFile: File)
}
