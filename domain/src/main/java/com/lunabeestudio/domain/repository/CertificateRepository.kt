/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.repository

import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import kotlinx.coroutines.flow.Flow

interface CertificateRepository {
    val rawWalletCertificatesFlow: Flow<TacResult<List<RawWalletCertificate>>>
    val certificateCountFlow: Flow<Int>
    suspend fun insertAllRawWalletCertificates(certificates: List<RawWalletCertificate>)
    suspend fun updateAllRawWalletCertificates(certificates: List<RawWalletCertificate>)
    suspend fun deleteRawWalletCertificate(certificateId: String)
    suspend fun deleteAllRawWalletCertificates()
    suspend fun getCertificateCount(): Int
    suspend fun getCertificateById(certificateId: String): RawWalletCertificate?
    suspend fun generateMultipass(serverPublicKey: String, encodedCertificateList: List<String>): TacResult<String>
    suspend fun forceRefreshCertificatesFlow()
    suspend fun deleteLostCertificates()
    fun resetKeyGeneratedFlag()
}
