/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/2 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.repository

import com.lunabeestudio.domain.model.TacResult

interface BlacklistRepository {
    suspend fun getMatchPrefix(hash: String): String?
    suspend fun getPrefixHashes(prefix: String): TacResult<List<String>>
}
